import { BrowserWindow, Certificate } from "electron";
import { None, Option, Some } from "ts-results";
import { periscopeIcon, trimTrailingSlash } from "./main-utils";
import { Observable } from "./observable";
import log from "electron-log/main";
import { showDialog } from "./dialog";

const logger = log.scope("CookieGrabber");

type MaybeInFlight = Option<Observable<string>>;

export class CookieGrabber {
    private _url: string;
    private inFlight: MaybeInFlight = None;

    constructor(url: string) {
        this._url = this.trimUrl(url);
        logger.info("Cookie Grabber made with", this._url);
    }

    set url(url: string) {
        this._url = this.trimUrl(url);
    }

    private trimUrl(url: string): string {
        url = url.trim();
        url = trimTrailingSlash(url);

        return url;
    }

    /**
     * Opens a browser and resolves a promise when the cookies have been retrieved.
     * Will reject if there is an error opening the window or the window is closed before
     * a cookie can be retrieved
     * @returns {Promise<string>}
     */
    grabCookie(): Promise<string> {
        const observable = this.getObservable();
        return observable.subscribe();
    }

    private getObservable(): Observable<string> {
        logger.debug("Getting an observable");
        if (this.inFlight.some) {
            return this.inFlight.val;
        }

        this.inFlight = new Some(this.makeInFlight());
        return this.inFlight.val;
    }

    private makeInFlight(): Observable<string> {
        logger.debug("Creating a browser window");
        const browserWindow = new BrowserWindow({
            width: 800,
            height: 600,
            icon: periscopeIcon(),
        });
        // Unfortunately this doesn't work on macOS and it will still have the full menu
        browserWindow.removeMenu();

        const certificateSelectorCallback = (
            event: { preventDefault: () => void },
            url: string,
            certificates: Certificate[],
            callback: (certs: Electron.Certificate) => void,
        ) => {
            event.preventDefault();
            logger.info("Certs requested for URL ", url);

            showDialog(
                "selector",
                {
                    question: "Select a Certificate",
                    values: certificates.map((cert) => {
                        return { title: cert.subjectName, subtitle: `Issued By: ${cert.issuerName}` };
                    }),
                },
                { parentWindow: browserWindow },
            )
                .then((result) => {
                    if (result.err) {
                        log.error("Failed to create certificate dialog", result.val);
                        callback(certificates[0]);
                        return;
                    }

                    const selected = result.val;
                    if (selected === undefined) {
                        callback(certificates[0]);
                        return;
                    }

                    callback(certificates[selected]);
                })
                .catch(() => {}); // This promise won't reject
        };

        browserWindow.webContents.on("select-client-certificate", certificateSelectorCallback);

        let hasReturned = false;
        const observable = new Observable<string>();

        browserWindow.webContents.on("did-finish-load", () => {
            const url = browserWindow.webContents.getURL();
            logger.info(`did-finish-load with ${url}, this: ${this._url}`);
            if (this.trimUrl(url) === this._url) {
                browserWindow.webContents.session.cookies
                    .get({
                        url: this._url,
                    })
                    .then((cookies) => {
                        if (cookies.length === 0) {
                            return;
                        }
                        hasReturned = true;

                        browserWindow.close();
                        const cookieString = cookies
                            .map((cookie) => {
                                return `${cookie.name}=${cookie.value}`;
                            })
                            .join("; ");

                        observable.pushValue(cookieString);
                    })
                    .catch((error) => observable.pushError(new Error(String(error))));
            }
        });

        browserWindow.on("closed", () => {
            if (!hasReturned) {
                observable.pushError(new Error("Window closed without a cookie"));
            }
        });

        browserWindow.loadURL(this._url).catch((error) => observable.pushError(new Error(String(error))));

        // rxjs has a .complete which we might want to emulate or just use rxjs instead of writing our own
        // This works for now though
        observable
            .subscribe()
            .finally(() => {
                logger.debug("Finally is called. Removing the inFlight");
                this.inFlight = None;

                // Need to check the browserWindow and webContents still exist before being able to set off
                if (browserWindow.isDestroyed()) return;
                if (browserWindow?.webContents?.isDestroyed()) return;

                browserWindow?.webContents?.off("select-client-certificate", certificateSelectorCallback);
                // This error will be caught elsewhere
            })
            .catch((e) => {
                logger.error("Failed to remove certificate callback", e);
            });

        return observable;
    }
}
