import { ImportedConditionSet } from "./server";
import { makeOption } from "./option";

export const Cancelled = makeOption<"cancel", void>("cancel");
export const FileData = makeOption<"file-data", ImportedConditionSet[]>("file-data");
export const FileError = makeOption<"error", string>("error");

export type ConditionImportResult =
    | ReturnType<typeof Cancelled>
    | ReturnType<typeof FileData>
    | ReturnType<typeof FileError>;
