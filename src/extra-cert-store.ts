import * as fs from 'node:fs';

/**
 * Store extra certs loaded from SSL_CERT_FILE environment
 */
export interface IExtraCertStore {
    /**
     * Returns whether the store contains extra certs
     */
    hasExtraCerts: () => boolean;
    /**
     * Returns extra certs if the store has any
     */
    extraCerts: () => string[] | undefined;
}

/**
 * Implementation of a Certificate Store
 */
export class ExtraCertStore implements IExtraCertStore {
    private _extraCerts?: string[];

    constructor() {
        const sslEnvironment = process.env.SSL_CERT_FILE;
        if (sslEnvironment != null && sslEnvironment !== "" && fs.existsSync(sslEnvironment)) {
            this.loadFromFile(sslEnvironment);
        }
    }

    /**
     * Loads the certificate from the file if the file has contents
     * @param filePath Path to load certs from
     */
    private loadFromFile(filePath: string) {
        const contents = fs.readFileSync(filePath);
        // TODO: Validate the contents contains a certificate
        if (contents.length > 0) {
            this._extraCerts = [contents.toString()];
        }
    }

    public extraCerts(): string[] | undefined {
        return this._extraCerts;
    }

    public hasExtraCerts(): boolean {
        return this._extraCerts !== null;
    }
}
