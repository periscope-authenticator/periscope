import { ConditionStore, ConditionStoreInput, ConditionStoreSchema } from "../http-condition/condition-store";
import { IpcRendererEvent } from "electron";
import { v4 } from "uuid";
import { z } from "zod";

export interface Server {
    id: string;
    name: string;
    url: string;
    localPort: string;
    useProxy: boolean;
    conditionSet: string;
    returnPath?: string;
    useTLS: boolean;
}

export const ConditionSetSchema = z.object({
    name: z.string(),
    conditions: ConditionStoreSchema.array(),
});

export type ConditionSetInput = z.infer<typeof ConditionSetSchema>;

// Would like to use ConditionSetInput. However ConditionStoreSchema does not have an id
// because we don't want to trust the external ids. However, there's not a nice way to
// tell Typescript that the inner property also needs an id.
export type ConditionSet = { id: string; isDefault: boolean; name: string; conditions: ConditionStore[] };
export type ImportedConditionSet = Omit<ConditionSet, "isDefault">;

const generateIdsForConditionStore = (conditionStore: ConditionStoreInput): ConditionStore => ({
    ...conditionStore,
    id: v4(),
});

export const generateIdsForConditionSet = (conditionSet: ConditionSetInput): ImportedConditionSet => ({
    ...conditionSet,
    conditions: conditionSet.conditions.map(generateIdsForConditionStore),
    id: v4(),
});

export interface ProxySettings {
    proxyUrl: string;
    tlsCertificatePath?: string;
    tlsKeyPath?: string;
    servers: Server[];
    conditionsSets: ConditionSet[];
}

export type StatusType = "running" | "stopped" | "error";

/**
 * Capitalizes the status for viewing
 * We know that StatusTypes are all one word so we can just capitalize the first char
 * @param status Status to capitalize
 * @returns capitalized version of the status
 */
export const capitalizeStatus = (status: StatusType) => {
    return status.charAt(0).toUpperCase() + status.slice(1);
};

export interface ServerStatus {
    server: Pick<Server, "id" | "name">;
    status: StatusType;
    message?: string;
}

export type ServerStatusCallback = (event: IpcRendererEvent, serverStatus: ServerStatus[]) => void;
