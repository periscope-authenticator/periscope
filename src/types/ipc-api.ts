import { DialogInput, DialogReturnUnion, DialogType } from "./dialog";
import { FileDialogOptions, FileDialogResult } from "./file-dialog";
import { IpcRendererEvent, ipcRenderer } from "electron";
import { ProxySettings, ServerStatus, ServerStatusCallback } from "./server";
import { ConditionImportResult } from "./import-file-result";
import { Result } from "ts-results";

// This is a kind of a workaround to get types into the api on the frontend so we need the any
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function invoke<Params extends any[], Return>(channel: string, ...args: Params) {
    return ipcRenderer.invoke(channel, ...args) as Promise<Return>;
}

// This is a kind of a workaround to get types into the api on the frontend so we need the any
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function on<Params extends any[]>(channel: string, callback: (event: IpcRendererEvent, ...args: Params) => void) {
    ipcRenderer.on(channel, callback);
}

export type WindowEvents = "show-cert-ui" | "new-server-status" | "show-about-menu";
export type ExportReturn = { cancelled: boolean };

const api = {
    saveSettings: (settings: ProxySettings) => ipcRenderer.invoke("saveSettings", settings),
    getSettings: () => invoke<[], ProxySettings>("getSettings"),
    emitUiLoaded: () => invoke<[], void>("ui-loaded"),
    getServerStatus: () => invoke<[], ServerStatus[]>("get-server-status"),
    onStatusChange: (callback: ServerStatusCallback) => on<[ServerStatus[]]>("new-server-status", callback),
    offStatusChange: (callback: ServerStatusCallback) => {
        ipcRenderer.off("new-server-status", callback);
    },
    startServer: (serverId: string) => invoke<[string], void>("start-server", serverId),
    stopServer: (serverId: string) => invoke<[string], void>("stop-server", serverId),
    exportConditionSets: (conditions: string[]) =>
        invoke<[string[]], Result<ExportReturn, string>>("export-condition-sets", conditions),
    importConditionSet: () => invoke<[], ConditionImportResult>("import-condition-set"),
    logFilePath: () => invoke<[], string>("log-file-path"),
    openLogFileInExplorer: () => invoke<[], void>("open-log-file"),
    onShowAboutMenu: (callback: () => void) => on<[]>("show-about-menu", callback),
    offShowAboutMenu: (callback: () => void) => ipcRenderer.off("show-about-menu", callback),
    showBrowseDialog: (dialogOptions: FileDialogOptions) =>
        invoke<[FileDialogOptions], FileDialogResult>("show-browse-dialog", dialogOptions),
    requestDialogData: (requestId: string) =>
        invoke<[string], DialogInput<DialogType>>("request-dialog-data", requestId),
    returnDialogData: (requestId: string, data: DialogReturnUnion) =>
        invoke<[string, DialogReturnUnion], void>("dialog-returned", requestId, data),
    registerDialogWindowSize: (requestId: string, rect: { width: number; height: number }) =>
        invoke<[string, { width: number; height: number }], void>("register-dialog-window-size", requestId, rect),
};

export default api;
