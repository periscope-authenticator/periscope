import * as React from "react";

import { Box, Button, ListItemButton, ListItemText, Typography } from "@mui/material";
import { useLoaderData, useParams } from "react-router-dom";
import { Dialog } from "./dialog";
import { DialogInput } from "../types/dialog";

export const SelectorDialog = () => {
    const input = useLoaderData() as DialogInput<"selector">;

    return <SelectorDialogInner {...input} />;
};

const SelectorDialogInner = ({ question, values }: DialogInput<"selector">) => {
    const [selected, setSelected] = React.useState<number | undefined>(undefined);

    const { requestId } = useParams();

    const returnSelectedValue = (selected: number | undefined) => {
        api.returnDialogData(requestId!, { type: "selector", value: selected }).catch(() => {});
    };

    return (
        <Dialog requestId={requestId!}>
            <Typography variant="h5" sx={{ textAlign: "left" }}>
                {question}
            </Typography>
            <Box>
                <ItemList items={values} selectedIndex={selected} onValueSelected={setSelected} />
            </Box>
            <Box sx={{ marginTop: "auto", float: "right", display: "flex", justifyContent: "flex-end" }}>
                <Button onClick={() => returnSelectedValue(undefined)}>Cancel</Button>
                <Button onClick={() => returnSelectedValue(selected)}>Select</Button>
            </Box>
        </Dialog>
    );
};

const ItemList = (props: {
    items: { title: string; subtitle?: string }[];
    selectedIndex: number | undefined;
    onValueSelected: (index: number) => void;
}) => {
    const { items, selectedIndex, onValueSelected } = props;

    if (items.length === 0) {
        return <>{"No items Passed"}</>;
    }

    return (
        <>
            {items.map((item, index) => {
                const callback = () => {
                    onValueSelected(index);
                };

                return <ListItem item={item} key={index} isSelected={selectedIndex === index} onClick={callback} />;
            })}
        </>
    );
};

const ListItem = (props: { item: { title: string; subtitle?: string }; isSelected: boolean; onClick: () => void }) => {
    const {
        item: { title, subtitle },
        isSelected,
        onClick,
    } = props;

    return (
        <ListItemButton selected={isSelected} onClick={onClick}>
            <ListItemText sx={{ width: "fit-content" }} primary={title} secondary={subtitle} />
        </ListItemButton>
    );
};
