type ChangeTarget = {
    value: string;
    checked?: boolean;
}

// TODO: Better model the HTMLInputElement | HTMLTextAreaElement so that we don't need ! for checked
export type ChangeEvent = {
    target: ChangeTarget
}

