import { HttpCondition, buildConditionsFromStore } from "./http-condition";
import { ProxySettings } from "../types/server";

export class HttpConditionManager {
    private conditionSets: Map<string, HttpCondition[]> = new Map();
    private defaultSet?: string;

    public updateSettings(settings: ProxySettings) {
        settings.conditionsSets.forEach((set) => {
            this.conditionSets.set(set.id, buildConditionsFromStore(set.conditions));
            if (set.isDefault) {
                this.defaultSet = set.id;
            }
        });
    }

    public getConditions(conditionSet: string): HttpCondition[] {
        if (conditionSet === "default" && this.defaultSet != null) {
            return this.conditionSets.get(this.defaultSet) ?? [];
        }

        return this.conditionSets.get(conditionSet) ?? [];
    }
}
