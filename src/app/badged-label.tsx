import * as React from 'react';

import { Badge, Chip } from '@mui/material';

interface BadgeProps {
    children: React.ReactNode,
    hasBadge: boolean,
    badgeContent?: React.ReactNode
    color?: BadgeColors
}

type BadgeColors = "default" | "secondary" | "info" | "warning" | "error" | "success" | "primary";

interface BadgeSubProps {
    color: BadgeColors,
    badgeContent?: React.ReactNode,
    variant?: "standard" | "dot",
}

/**
 * Builds a label with an optional badge icon above it
 * @param props props for the label
 * @returns
 */
export const BadgedLabel = (props: BadgeProps) => {
    if (!props.hasBadge) return (<> {props.children} </>);

    const subProps: BadgeSubProps = {
        color: props.color ?? 'secondary'
    };

    subProps.badgeContent = props.badgeContent ?? ' ';

    if (props.badgeContent === null || props.badgeContent === undefined) {
        subProps.variant = 'dot';
    }

    return (
        <Badge {...subProps}>
            {props.children}
        </Badge >
    );
};

interface ChipProps {
    children: React.ReactNode,
    hasChip: boolean,
    chipContent?: React.ReactNode,
    color?: BadgeColors,
    size?: 'small' | 'medium',
}

export const ChippedLabel = (props: ChipProps) => {
    const { children, hasChip, chipContent, color, size } = props;
    return (
        <>
            {children}
            {
                hasChip &&
                <Chip label={chipContent} color={color} size={size} />
            }
        </>
    );
};
