import * as React from "react";
import { Box, Stack, StackProps } from "@mui/material";

interface TabProps extends StackProps {
  selectedIndex: number;
  index: number;
}

function TabPanel(props: TabProps) {
  const { children, selectedIndex, index, ...other } = props;

  return (
    <Stack
      role="tabpanel"
      hidden={selectedIndex !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {selectedIndex === index && (
        <Box sx={{ p: 3 }}>
          {children}
        </Box>
      )}
    </Stack>
  );
}

export { TabPanel };
