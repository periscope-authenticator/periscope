import * as React from "react";
import { Box, Button, TextField, Typography } from "@mui/material";
import { useLoaderData, useParams } from "react-router-dom";
import { Dialog } from "./dialog";
import { DialogInput } from "../types/dialog";

export const TextInputDialog = () => {
    const input = useLoaderData() as DialogInput<"textInput">;

    return <TextInputDialogInner {...input} />;
};

const TextInputDialogInner = ({ question, inputType }: DialogInput<"textInput">) => {
    const { requestId } = useParams();

    const [text, setText] = React.useState("");

    const returnText = (value: string | undefined) => {
        api.returnDialogData(requestId!, { type: "textInput", value }).catch(() => {});
    };

    return (
        <Dialog
            requestId={requestId as string}
            sx={{
                minWidth: 500,
                minHeight: 150,
            }}
        >
            <Typography variant="h5" sx={{ textAlign: "left" }}>
                {question}
            </Typography>
            <TextField
                onChange={(event) => setText(event.target.value)}
                onKeyDown={(event) => {
                    if (event.key === "Enter") {
                        returnText(text);
                        event.preventDefault();
                    }
                }}
                value={text}
                type={inputType}
            />

            <Box sx={{ float: "right", display: "flex", justifyContent: "flex-end" }}>
                <Button onClick={() => returnText(undefined)}>Cancel</Button>
                <Button onClick={() => returnText(text)}>Select</Button>
            </Box>
        </Dialog>
    );
};
