import { BrowserWindow, dialog, ipcMain } from "electron";
import { Err, Ok, Result } from "ts-results";
import EventEmitter from "events";
import { WindowEvents } from "./types/ipc-api";
import log from "electron-log/main";
import { periscopeIcon } from "./main-utils";
import { withTimeout } from "./utils";

declare const MAIN_WINDOW_WEBPACK_ENTRY: string;
declare const MAIN_WINDOW_PRELOAD_WEBPACK_ENTRY: string;

const GUI_LOADING_TIMEOUT = 30000; // in ms

export interface GuiWindowManager {
    activateOrCreate(): Promise<void>;
    // The underlying send command does not enforce any types for it's arguments
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    sendToWindow(channel: WindowEvents, ...args: any[]): void;
    openSaveDialog(options?: Electron.SaveDialogOptions): Promise<Result<Electron.SaveDialogReturnValue, Error>>;
}

const logger = log.scope("GuiWindowManager");

export class GuiWindowManagerImpl extends EventEmitter implements GuiWindowManager {
    private window?: BrowserWindow;
    constructor() {
        super();
        ipcMain.handle("ui-loaded", () => {
            this.emit("ui-loaded");
        });
    }

    openOpenFileDialog(options?: Electron.OpenDialogOptions): Promise<Result<Electron.OpenDialogReturnValue, Error>> {
        const window = this.window?.isDestroyed() ? null : this.window;

        return (
            dialog
                // @ts-expect-error The documentation says window is optional but the types do not reflect that.
                // https://www.electronjs.org/docs/latest/api/dialog#dialogshowopendialogbrowserwindow-options
                .showOpenDialog(window, options)
                .then((val) => Ok(val))
                .catch(() => Err(new Error("Failed to open save dialog")))
        );
    }

    openSaveDialog(options?: Electron.SaveDialogOptions): Promise<Result<Electron.SaveDialogReturnValue, Error>> {
        const window = this.window?.isDestroyed() ? null : this.window;

        return (
            dialog
                // @ts-expect-error The documentation says window is optional but the types do not reflect that.
                // https://www.electronjs.org/docs/latest/api/dialog#dialogshowsavedialogbrowserwindow-options
                .showSaveDialog(window, options)
                .then((val) => Ok(val))
                .catch(() => Err(new Error("Failed to open save dialog")))
        );
    }

    private createWindow() {
        logger.info("Creating a new BrowserWindow");
        this.window = new BrowserWindow({
            width: 800,
            height: 600,
            icon: periscopeIcon(),
            show: false,
            webPreferences: {
                preload: MAIN_WINDOW_PRELOAD_WEBPACK_ENTRY,
            },
        });
    }

    private async loadGui() {
        if (this.window == null) return;

        await this.window
            .loadURL(MAIN_WINDOW_WEBPACK_ENTRY)
            .catch((err) => logger.error("Failed to load Main GUI", err));

        await this.awaitGuiLoads();
        this.window.show();
    }

    private awaitGuiLoads(): Promise<void> {
        return withTimeout<void>(
            new Promise((resolve) => {
                this.once("ui-loaded", resolve);
            }),
            GUI_LOADING_TIMEOUT
        );
    }

    private async createWindowAndLoad() {
        this.createWindow();
        await this.loadGui();
    }

    public async activateOrCreate() {
        if (this.window == null || this.window.isDestroyed()) {
            await this.createWindowAndLoad();
        } else {
            this.window.focus();
        }
    }

    // The underlying send command does not enforce any types for it's arguments
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public sendToWindow(channel: WindowEvents, ...args: any[]) {
        if (this.window == null || this.window.isDestroyed()) {
            log.warn("Attempted to send event", channel, "to window but window is not available");
            return;
        }

        // Send does not provide any typing for the arguments
        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
        this.window.webContents.send(channel, ...args);
    }
}
