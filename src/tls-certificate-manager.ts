import { None, Option, Some } from "ts-results";
import EventEmitter from "events";
import { ServerStore } from "./server-store";
import { isBlank } from "./utils";
import log from 'electron-log/main';
import { readFileSync } from "fs";

const logger = log.scope("TLSCertificateManager");

export class TLSCertificateManager extends EventEmitter {
    private tlsCertificatePath?: string;
    private tlsKeyPath?: string;

    private tlsCertificate?: Buffer;
    private tlsKey?: Buffer;
    private currentError?: string;
    constructor(storage: ServerStore) {
        super();
        const {tlsCertificatePath, tlsKeyPath} = storage.getAll();

        this.setCertificateAndKeyPath(tlsCertificatePath, tlsKeyPath);

        storage.watchForChanges((storage) => {
            logger.debug("Got changes in TLSCertManager");
            if (!storage) return;

            if (storage.tlsKeyPath !== this.tlsKeyPath || storage.tlsCertificatePath !== this.tlsCertificatePath) {
                this.setCertificateAndKeyPath(storage.tlsCertificatePath, storage.tlsKeyPath);
            }
        });
    }

    public certAndKey(): Option<{ cert: Buffer, key: Buffer }> {
        if (isBlank(this.tlsCertificate) || isBlank(this.tlsKey)) {
            return None;
        }

        // We know it's not null because of the above check
        return Some({ cert: this.tlsCertificate!, key: this.tlsKey! });
    }

    public get error() {
        return this.currentError;
    }


    private setCertificateAndKeyPath(tlsCertificatePath: string | undefined, tlsKeyPath: string | undefined) {
        if (!isBlank(tlsCertificatePath) && !isBlank(tlsKeyPath)) {
            this.tlsCertificatePath = tlsCertificatePath;
            this.tlsKeyPath = tlsKeyPath;

            this.loadCertificateAndKey();
        }
    }

    private loadCertificateAndKey() {
        if (isBlank(this.tlsCertificatePath) || isBlank(this.tlsKeyPath)) return;

        try {
            this.tlsCertificate = readFileSync(this.tlsCertificatePath!);
            this.tlsKey = readFileSync(this.tlsKeyPath!);
            this.emit('new-cert-loaded');
        } catch (e) {
            this.tlsCertificate = undefined;
            this.tlsKey = undefined;
            logger.error('Failed to open certificate or key file:', e);
            this.emit('error', e);
        }
    }
}
