import * as React from "react";
import { Box, CircularProgress } from "@mui/material";
import { ProxySettings, ServerStatus } from "../types/server";
import Settings from "./settings";
import log from "electron-log/renderer";

interface RootState {
    settings?: ProxySettings;
    statuses?: ServerStatus[];
}

export const Main = () => {
    const [rootState, setRootState] = React.useState<RootState>({});

    React.useEffect(() => {
        const getSettings = async () => {
            const settings = await api.getSettings();

            setRootState((state) => {
                return { ...state, settings };
            });
        };

        const getStatuses = async () => {
            const statuses = await api.getServerStatus();

            setRootState((state) => {
                return { ...state, statuses };
            });
        };

        getSettings().catch((err) => {
            log.error("An error has occurred getting settings", String(err));
        });

        getStatuses().catch((err) => {
            log.error("An error occurred getting statuses", String(err));
        });
    }, []);

    if (!rootState.settings) {
        return (
            <Box>
                <CircularProgress />
            </Box>
        );
    }

    return <Settings initialSettings={rootState.settings} initialStatus={rootState.statuses} />;
};
