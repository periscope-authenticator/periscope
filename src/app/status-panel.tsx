import * as React from "react";

import {
    Box,
    Container,
    IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableRow,
} from "@mui/material";
import { ServerStatus, capitalizeStatus } from "../types/server";
import { NotificationContext } from "./notification-context";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import StopIcon from "@mui/icons-material/Stop";

interface StatusProps {
    statuses: ServerStatus[];
}

export const StatusPanel = (props: StatusProps) => {
    return (
        <Container>
            <ServerTable {...props} />
        </Container>
    );
};

const ServerTable = (props: StatusProps) => {
    const { statuses } = props;

    return (
        <TableContainer component={Paper}>
            <Table>
                <TableBody>
                    {statuses.map((status) => (
                        <ServerRow {...status} key={status.server.id} />
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
};

const ServerRow = (server: ServerStatus) => {
    return (
        <TableRow>
            <TableCell sx={{ fontWeight: "bold" }}>{server.server.name}</TableCell>
            <TableCell>
                <Box sx={{ display: "flex", alignItems: "center" }}>
                    <StatusLight {...server} /> {capitalizeStatus(server.status)}
                    <br />
                    {server.message}
                </Box>
            </TableCell>
            <TableCell>
                <ServerControls status={server} />
            </TableCell>
        </TableRow>
    );
};

const StatusLight = (serverStatus: ServerStatus) => {
    const { status } = serverStatus;

    const color = {
        running: "success.main",
        error: "error.main",
        stopped: "warning.main",
    }[status];

    return (
        <Box
            sx={{
                backgroundColor: color,
                borderRadius: "50%",
                width: "1em",
                height: "1em",
                display: "inline-block",
                margin: "1em 1em",
            }}
        ></Box>
    );
};

const ServerControls = ({ status }: { status: ServerStatus }) => {
    const { status: statusType, server } = status;
    const notification = React.useContext(NotificationContext);

    const onStartClicked = React.useCallback(() => {
        api.startServer(server.id).catch((err: Error) =>
            notification.notify(`Failed to start server ${String(err)}`, "error")
        );
    }, [server, notification]);

    const onStopClicked = React.useCallback(() => {
        api.stopServer(server.id).catch((err: Error) =>
            notification.notify(`Failed to stop server ${String(err)}`, "error")
        );
    }, [server, notification]);

    return (
        <>
            <IconButton
                aria-label="start"
                disabled={statusType === "running" || statusType === "error"}
                onClick={onStartClicked}
            >
                <PlayArrowIcon />
            </IconButton>
            <IconButton
                aria-label="stop"
                disabled={statusType === "stopped" || statusType === "error"}
                onClick={onStopClicked}
            >
                <StopIcon />
            </IconButton>
        </>
    );
};
