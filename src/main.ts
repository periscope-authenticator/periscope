import { App } from './app';
import { app as electronApp } from "electron";
import log from 'electron-log/main';

if (require("electron-squirrel-startup")) electronApp.quit();

// app is not unused we need it to keep the entire app in memory
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const app = new App();

log.initialize();
