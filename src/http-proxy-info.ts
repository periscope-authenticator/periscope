import { None, Option, Some } from "ts-results";
import { isBlank } from "./utils";

export type HttpProxyInfo = {
    host: () => string;
    port: () => string;
    protocol: () => string;
    authenticationHeader: () => Option<string>;
};

type AuthenticationData = {
    username?: string;
    password?: string;
};

export class HttpProxyInfoImpl implements HttpProxyInfo {
    private readonly _host: string;
    private readonly _port: string;
    private readonly _protocol: string;
    private readonly authenticationData: Option<AuthenticationData>;

    constructor(proxyUrl: URL) {
        this._host = proxyUrl.host;
        this._port = proxyUrl.port;
        this._protocol = proxyUrl.protocol;
        if (!isBlank(proxyUrl.username) || !isBlank(proxyUrl.password)) {
            this.authenticationData = Some({ username: proxyUrl.username, password: proxyUrl.password });
        } else {
            this.authenticationData = None;
        }
    }

    host() {
        return this._host;
    }

    port() {
        if (isBlank(this._port)) {
            if (this._protocol === "http:") {
                return "80";
            }
            if (this._protocol === "https:") {
                return "443";
            }
            return "";
        }

        return this._port;
    }

    protocol() {
        return this._protocol;
    }

    authenticationHeader() {
        if (this.authenticationData.none) {
            return None;
        }

        const authData = this.authenticationData.val;

        const concatAuth = `${authData.username}:${authData.password}`;

        return Some(`Basic ${Buffer.from(concatAuth).toString("base64")}`);
    }
}
