export type DialogType = keyof DialogReturnMap;
export type DialogReturn<T extends DialogType> = DialogReturnMap[T];
export type DialogReturnUnion =
    | {
          type: "textInput";
          value: DialogReturnMap["textInput"];
      }
    | {
          type: "selector";
          value: DialogReturnMap["selector"];
      };
export type DialogInput<T extends DialogType> = DialogInputMap[T];

type DialogInputMap = {
    textInput: { question: string; inputType: "text" | "password" };
    selector: { question: string; values: { title: string; subtitle?: string }[] };
};

type DialogReturnMap = {
    textInput: string | undefined;
    selector: number | undefined;
};
