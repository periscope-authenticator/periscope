import * as http from "node:http";

import { isBlank, realmFromWWWAuthenticateHeader, tryIntoUrl } from "./utils";

import { IServerManager } from "./server-manager";
import { ServerInfo } from "./proxy-server";

import packageJson from "../package.json";

export const PERISCOPE_USER_AGENT = `${packageJson.name}/${packageJson.version}` as const;

export class HeaderTransform {
    private cookies = "";
    private serverInfo?: ServerInfo;

    constructor(private serverManager: IServerManager) { }

    public onNewCookies(newCookies: string) {
        this.cookies = newCookies;
    }

    public newServerInfo(newServerInfo: ServerInfo) {
        this.serverInfo = newServerInfo;
    }

    public transformHeadersForRemoteRequest(receivedHeaders: http.IncomingHttpHeaders): http.IncomingHttpHeaders {
        return {
            ...receivedHeaders,
            cookie: this.buildCookieString(),
            host: this.serverInfo?.proxyAddress,
            "user-agent": this.buildUserAgentString(receivedHeaders),
        };
    }

    public transformHeaderForClientResponse(
        originalClientHeaders: http.IncomingHttpHeaders,
        serverResponseHeaders: http.IncomingHttpHeaders
    ): http.IncomingHttpHeaders {

        const output = { ...serverResponseHeaders };
        const location = this.buildLocationHeader(serverResponseHeaders, originalClientHeaders);
        if (location != null) {
            output.location = location;
        }

        const authenticate = this.buildWWWAuthenticateHeader(serverResponseHeaders, originalClientHeaders);
        if (authenticate != null) {
            output['www-authenticate'] = authenticate;
        }

        return output;
    }

    private buildUserAgentString(origHeaders: http.IncomingHttpHeaders): string {
        const currentUserAgent = origHeaders["user-agent"];
        if (currentUserAgent == null) return PERISCOPE_USER_AGENT;

        return `${currentUserAgent} ${PERISCOPE_USER_AGENT}`;
    }

    private buildCookieString(): string {
        const cookies = this.cookies;
        // TODO: This could merge with the real cookies?
        return cookies;
    }

    private buildWWWAuthenticateHeader(
        headersFromServer: http.IncomingHttpHeaders,
        clientRequestHeaders: http.IncomingHttpHeaders
    ): string | undefined {
        const currentWWWAuth = headersFromServer["www-authenticate"];
        if (isBlank(currentWWWAuth)) return undefined;

        const realmInfo = realmFromWWWAuthenticateHeader(currentWWWAuth);

        if (realmInfo == null) return currentWWWAuth;

        const { realm, realmStart, realmEnd } = realmInfo;
        const hostFromClient = clientRequestHeaders.host ?? "localhost";
        const url = tryIntoUrl(realm);
        if (url == null) return currentWWWAuth;

        const newUrl = this.serverManager.localForRemoteAddress(url, hostFromClient);

        // isBlank checks for undefined
        return currentWWWAuth!.substring(0, realmStart) + newUrl + currentWWWAuth!.substring(realmEnd);
    }

    private buildLocationHeader(
        headersFromServer: http.IncomingHttpHeaders,
        clientRequestHeaders: http.IncomingHttpHeaders
    ): string | undefined {
        const currentLocation = headersFromServer.location;
        if (isBlank(currentLocation)) return undefined;

        // isBlank checks for undefined
        const url = tryIntoUrl(currentLocation!);
        if (url == null) return currentLocation;

        const newURL = this.serverManager.localForRemoteAddress(url, clientRequestHeaders.host ?? "localhost");

        return newURL;
    }
}
