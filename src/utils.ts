import * as React from "react";
import { Err, Ok, Result } from "ts-results";
import log from "electron-log";
import { z } from "zod";

/**
 * Groups the array by the keys returned by keyFunction
 * @param array array to group
 * @param keyFunction function to group by
 * @returns a map based on the keys returned by the keyFunction
 */
function groupArrayBy<T, K>(array: T[], keyFunction: (item: T) => K): Map<K, T[]> {
    return array.reduce((entryMap, e) => {
        const key = keyFunction(e);
        return entryMap.set(key, [...(entryMap.get(key) ?? []), e]);
    }, new Map<K, T[]>());
}

interface HasLength {
    length: number;
}

/**
 * Checks if the input is blank meaning null, undefined or a length of 0
 * @param input type with a length
 * @returns whether the value is blank
 */
function isBlank(input: HasLength | undefined | null): boolean {
    return input === null || input === undefined || input.length === 0;
}

function useDidUpdateEffect(fn: React.EffectCallback, depends: React.DependencyList) {
    const didMountRef = React.useRef(false);

    React.useEffect(() => {
        if (didMountRef.current) {
            return fn();
        }
        didMountRef.current = true;
        // This is in itself a hook so we don't need the checking
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, depends);
}

function isNumeric(input: string): boolean {
    const n = Math.floor(Number(input));
    return n !== Infinity && !Number.isNaN(n);
}

interface WithID {
    id: string;
}

type MappingFunction<T> = (obj: T) => T;

/**
 * Returns a function which will replace the passed object if the id properties match
 * @param objectToReplace object to replace with
 * @returns
 */
function replaceWithObject<T extends WithID>(objectToReplace: T): MappingFunction<T> {
    return (current: T) => {
        return current.id === objectToReplace.id ? objectToReplace : current;
    };
}

function withTimeout<T>(promise: Promise<T>, ms: number): Promise<T> {
    let timer: NodeJS.Timeout;
    return Promise.race([promise, new Promise<T>((_, rej) => (timer = setTimeout(rej, ms)))]).finally(() =>
        clearTimeout(timer),
    );
}

interface RealmData {
    realm: string;
    realmStart: number;
    realmEnd: number;
}

function realmFromWWWAuthenticateHeader(headerValue: string | undefined | null): RealmData | null {
    if (headerValue == null) return null;

    const urlRegex = new RegExp(/realm="(?<url>[^"]+)"/, "d");

    const regexRes = urlRegex.exec(headerValue);
    if (regexRes == null) return null;
    if (regexRes.indices?.groups?.url == null) return null;
    if (regexRes.groups?.url == null) return null;

    const [realmStart, realmEnd] = regexRes.indices.groups.url;

    return {
        realm: regexRes.groups.url,
        realmStart,
        realmEnd,
    };
}

function tryIntoUrl(input: string): URL | null {
    try {
        return new URL(input);
    } catch {
        return null;
    }
}

function trimLeadingSlash(input: string): string {
    if (input.startsWith("/")) return input.slice(1);

    return input;
}

function trimTrailingSlash(input: string): string {
    if (input.endsWith("/")) return input.substring(0, input.length - 1);

    return input;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function safeJSONParse(text: string): Result<any, string> {
    try {
        return Ok(JSON.parse(text));
    } catch (e) {
        return Err(`Failed to parse JSON ${String(e)}`);
    }
}

function parseAndValidateJSON<T>(text: string, schema: z.ZodType<T>): Result<T, string> {
    const parsedJSON = safeJSONParse(text);
    if (parsedJSON.err) {
        return parsedJSON;
    }

    const zodParse = schema.safeParse(parsedJSON.val);
    if (zodParse.success) return Ok(zodParse.data);

    log.error("Failed to parse JSON", zodParse.error);

    return Err("Invalid JSON: does not conform to schema");
}

/**
 * Removes the null from a type and leaves undefined.
 * Useful for transfers between zod and React since the nullish action
 * is the only one that allows missing and explicit null but also produces a null type
 * @param value value to drop null from
 */
function dropNull<T>(value: T | undefined | null): T | undefined {
    return value === null ? undefined : value;
}

export {
    dropNull,
    isBlank,
    isNumeric,
    groupArrayBy,
    useDidUpdateEffect,
    parseAndValidateJSON,
    realmFromWWWAuthenticateHeader,
    replaceWithObject,
    safeJSONParse,
    trimLeadingSlash,
    trimTrailingSlash,
    tryIntoUrl,
    withTimeout,
};
