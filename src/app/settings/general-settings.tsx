import * as React from 'react';
import { ActionValue, OnValidated, defaultValidator } from '../settings';
import { Button, Stack, TextField, Typography } from '@mui/material';
import { NotificationContext } from '../notification-context';
import { ValidatedField } from '../validated-field';

export interface ProxyStateBase {
    proxyUrl: ActionValue<string>;
    tlsCertificate: ActionValue<string | undefined>;
    tlsKey: ActionValue<string | undefined>;
}
interface ProxyState extends ProxyStateBase {
    onValidated: OnValidated;
}

export function GeneralSettings(props: ProxyState) {
    const { onValidated, tlsCertificate, tlsKey } = props;

    const validation = React.useCallback(
        (input: HTMLInputElement) => {
            const validation = defaultValidator(input);

            onValidated(validation.isValid);

            return validation;
        },
        [onValidated],
    );
    const notifyContext = React.useContext(NotificationContext);

    const onCertBrowseClicked = () => {
        api.showBrowseDialog({ title: 'TLS Certificate', files: [{ name: 'Certificate', extensions: ['crt', 'pem'] }], multiselect: false })
            .then((result) => {
                switch (result._type) {
                    case 'cancel':
                        return;
                    case 'file-path':
                        tlsCertificate.onChange({ target: { value: result.value } });
                        break;
                    case 'multi-file-path':
                        tlsCertificate.onChange({ target: { value: result.value[0] } });
                        break;
                    case 'error':
                        notifyContext.notify(`Failed to open file dialog ${result.value}`, 'error');
                        break;
                }
            })
            .catch(() => notifyContext.notify('Failed to open file dialog', 'error'));
    };

    const onKeyBrowseClicked = () => {
        api.showBrowseDialog({ title: 'TLS Key', files: [{ name: 'Key', extensions: ['key', 'pem'] }], multiselect: false })
            .then((result) => {
                switch (result._type) {
                    case 'cancel':
                        return;
                    case 'file-path':
                        tlsKey.onChange({ target: { value: result.value } });
                        break;
                    case 'multi-file-path':
                        tlsKey.onChange({ target: { value: result.value[0] } });
                        break;
                    case 'error':
                        notifyContext.notify(`Failed to open file dialog ${result.value}`, 'error');
                        break;
                }
            })
            .catch(() => notifyContext.notify('Failed to open file dialog', 'error'));
    };
    return (
        <Stack spacing={3}>
            <ValidatedField
                id="proxyUrl"
                label="Proxy URL in the form http://host:port"
                onChange={props.proxyUrl.onChange}
                value={props.proxyUrl.value}
                helperText="Used to connect to servers"
                type="url"
                validateCallback={validation}
            />
            <Stack direction="row" spacing={2}>
                <TextField value={tlsCertificate.value} fullWidth onChange={tlsCertificate.onChange} helperText="TLS Certificate" label="Certificate Used when enabling TLS" />
                <Button onClick={onCertBrowseClicked} variant="text" sx={{ marginBottom: '1em' }}>
                    Browse
                </Button>
            </Stack>
            <Stack direction="row" spacing={2}>
                <TextField fullWidth value={tlsKey.value} onChange={tlsKey.onChange} helperText="TLS Key" label="Key used when enabling TLS" />
                <Button onClick={onKeyBrowseClicked} variant="text">
                    Browse
                </Button>
            </Stack>
            <Typography variant='subtitle1' color='error' textAlign='start'>
                NOTE: Private Keys with a passphrase are not supported
            </Typography>
        </Stack>
    );
}
