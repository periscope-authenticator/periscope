import { BrowserWindow, ipcMain, screen } from "electron";
import { DialogInput, DialogReturn, DialogReturnUnion, DialogType } from "./types/dialog";
import { Err, Ok, Result } from "ts-results";
import log from "electron-log/main";
import { v4 as uuid } from 'uuid';

declare const MAIN_WINDOW_WEBPACK_ENTRY: string;
declare const MAIN_WINDOW_PRELOAD_WEBPACK_ENTRY: string;

type DialogOptions = Partial<DialogOptionsFull>;

interface DialogOptionsFull {
    parentWindow?: BrowserWindow;
    resizable: boolean;
    resizeToRenderedSize: boolean;
    borderless: boolean;
    title?: string;
}

export async function showDialog<T extends DialogType>(
    dialogType: T,
    dialogInput: DialogInput<T>,
    dialogOptions: DialogOptions,
): Promise<Result<DialogReturn<T>, string>> {
    const requestId = uuid();
    const filledOptions: DialogOptionsFull = {
        resizable: false,
        resizeToRenderedSize: true,
        borderless: true,
        ...dialogOptions,
    };

    DialogIpc.get().registerHandler("request-dialog-data", requestId, () => dialogInput);

    const windowResult = await showWindow(dialogType, requestId, filledOptions);

    if (windowResult.err) {
        return windowResult;
    }

    const window = windowResult.val;

    const dialogReturn = await waitForDialogReturn(dialogType, requestId);
    window.destroy();

    return dialogReturn;
}

function waitForDialogReturn<T extends DialogType>(
    dialogType: T,
    requestId: string,
): Promise<Result<DialogReturn<T>, string>> {
    return new Promise((resolve) => {
        DialogIpc.get().registerHandler("dialog-returned", requestId, (value) => {
            if (value.type !== dialogType) {
                resolve(Err(`Incorrect return type from UI wanted ${dialogType}. got ${value.type}`));
                return;
            }

            resolve(Ok(value.value as DialogReturn<T>));
        });
    });
}

async function showWindow(
    dialogType: DialogType,
    requestId: string,
    dialogOptions: DialogOptionsFull,
): Promise<Result<BrowserWindow, string>> {
    const [parentWidth, parentHeight] = windowSize(dialogOptions.parentWindow);

    const newWindow = new BrowserWindow({
        parent: dialogOptions.parentWindow,
        frame: !dialogOptions.borderless,
        resizable: dialogOptions.resizable,
        // If we are going to resize we don't want to show initially
        show: !dialogOptions.resizeToRenderedSize,
        width: parentWidth,
        height: parentHeight,
        webPreferences: {
            preload: MAIN_WINDOW_PRELOAD_WEBPACK_ENTRY,
        },
    });

    const [parentX, parentY] = dialogOptions.parentWindow?.getPosition() ?? [0, 0];

    if (dialogOptions.resizeToRenderedSize) {
        DialogIpc.get().registerHandler("register-dialog-window-size", requestId, ({ width, height }) => {
            const newPos = centerRelativeToParent(
                { width: parentWidth, height: parentHeight, x: parentX, y: parentY },
                width,
                height,
            );
            newWindow.setBounds({ ...newPos, width, height });
            newWindow.show();
        });
    }

    const err = await newWindow
        .loadURL(addPathToWebpack(`${dialogType}/${requestId}`))
        .then(() => Ok.EMPTY)
        .catch(() => Err("Failed to load window"));

    if (err.err) return err;

    return Ok(newWindow);
}

function addPathToWebpack(path: string): string {
    const pathSeparator = "#";

    return `${MAIN_WINDOW_WEBPACK_ENTRY}${pathSeparator}${path}`;
}

function windowSize(parentWindow?: BrowserWindow): [number, number] {
    if (parentWindow) {
        const [width, height] = parentWindow.getSize();
        return [width, height];
    }

    // If we don't have a parent window we wanna give the UI as much space as needed
    const primaryDisplay = screen.getPrimaryDisplay();
    const { width, height } = primaryDisplay.workAreaSize;

    return [width, height];
}

function centerRelativeToParent(
    parentBounds: { x: number; y: number; width: number; height: number },
    newWidth: number,
    newHeight: number,
): { x: number; y: number } {
    const newX = parentBounds.x + parentBounds.width / 2 - newWidth / 2;
    const newY = parentBounds.y + parentBounds.height / 2 - newHeight / 2;

    return { x: Math.ceil(newX), y: Math.ceil(newY) };
}

type Handlers = {
    "request-dialog-data": { [key: string]: () => DialogInput<DialogType> };
    "dialog-returned": { [key: string]: (value: DialogReturnUnion) => void };
    "register-dialog-window-size": { [key: string]: (value: { width: number; height: number }) => void };
};

type Events = keyof Handlers;

type HandlerCallbackType<EventType extends Events> = Handlers[EventType][string];
type HandlerCallbackParams<EventType extends Events> = Parameters<HandlerCallbackType<EventType>>;

class DialogIpc {
    private static ipc?: DialogIpc;

    public static get(): DialogIpc {
        DialogIpc.ipc ??= new DialogIpc();
        return DialogIpc.ipc;
    }

    private handlers: Handlers = {
        "request-dialog-data": {},
        "dialog-returned": {},
        "register-dialog-window-size": {},
    };

    constructor() {
        this.registerIpc("request-dialog-data");
        this.registerIpc("dialog-returned");
        this.registerIpc("register-dialog-window-size");
    }

    private registerIpc<T extends Events, EventParams extends HandlerCallbackParams<T> = HandlerCallbackParams<T>>(
        event: T,
    ) {
        ipcMain.handle(event, (_: Electron.IpcMainInvokeEvent, requestId: string, ...callbackParams: EventParams) => {
            log.info("Received ipc for type", event, "with request", requestId);
            const handler = this.handlers[event][requestId] as HandlerCallbackType<T>;
            if (!handler) return;

            // @ts-expect-error all the type check out for this to function properly but Typescript doesn't like narrowing the type here
            // eslint-disable-next-line prefer-spread
            return handler.apply(null, callbackParams);
        });
    }

    registerHandler<T extends Events>(event: T, requestId: string, callback: HandlerCallbackType<T>) {
        log.info("handler registered for event", event, "and request", requestId);
        this.handlers[event][requestId] = callback;
    }
}
