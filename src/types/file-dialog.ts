import { FileError } from "./import-file-result";
import { makeOption } from "./option";

export type FileTypes = {
    name: string;
    extensions: string[];
}

export type FileDialogOptions = {
    title: string;
    files: FileTypes[];
    multiselect: boolean;
};

export const Cancelled = makeOption<"cancel", void>("cancel");
export const FilePath = makeOption<"file-path", string>("file-path");
export const MultiFilePath = makeOption<"multi-file-path", string[]>("multi-file-path");
export const DialogError = makeOption<"error", string>("error");

export type FileDialogResult =
    | ReturnType<typeof Cancelled>
    | ReturnType<typeof FilePath>
    | ReturnType<typeof MultiFilePath>
    | ReturnType<typeof FileError>;
