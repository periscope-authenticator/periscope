import { ConditionSources, ConditionStore } from "./http-condition/condition-store";
import ElectronStore from "electron-store";
import { ProxySettings } from "./types/server";
import { v4 as uuid } from 'uuid';

type WatchStoreCallback = (newStore: ProxySettings | undefined, oldStore: ProxySettings | undefined) => void;

/**
 * Wrapper around the ElectronStore config file
 */
export class ServerStore {
    private store: ElectronStore<ProxySettings>;

    constructor() {
        this.store = new ElectronStore<ProxySettings>({
            defaults: {
                servers: [
                    {
                        name: "Default",
                        useProxy: false,
                        id: uuid(),
                        localPort: "21000",
                        url: "http://example.com",
                        conditionSet: "default",
                        useTLS: false,
                    },
                ],
                proxyUrl: "",
                conditionsSets: [
                    {
                        id: uuid(),
                        isDefault: true,
                        conditions: [],
                        name: "Conditions",
                    },
                ],
            },
            migrations: {
                "0.0.1": (store) => {
                    // Need to have this legacy filter interface locally so that both filter and condition are available
                    // This is pretty niche since most people will never have the filter nomenclature
                    interface LegacyFilter extends ConditionStore {
                        filterSource: ConditionSources;
                    }

                    if (store.has("filters")) {
                        // The rule was incorrect and Typescript was erroring without the type assertion
                        // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
                        const existingConditions = store.get("filters") as LegacyFilter[];

                        const newConditions = existingConditions.map((condition: LegacyFilter) => {
                            condition.conditionSource = condition.filterSource;
                            // @ts-expect-error Typescript doesn't like it since the types are changing
                            delete condition.filterSource;
                            return condition;
                        });

                        // @ts-expect-error the field used to be called filters so we wanna copy anything from there
                        store.delete("filters");

                        store.set("conditions", newConditions);
                    }
                },
                "0.1.0": (store) => {
                    if (!store.has("conditions")) return;

                    // Move all the current conditions into a set called Default
                    const conditions = store.get("conditions");

                    const newConditionSet = {
                        name: "Default",
                        isDefault: true,
                        conditions,
                    };

                    store.set("conditionSets", [newConditionSet]);

                    // @ts-expect-error the type no longer has the conditions field but we still should delete it
                    store.delete("conditions");

                    // Updating all servers to use default
                    const servers = store.get("servers");
                    const updatedServers = servers.map((server) => {
                        server.conditionSet = "default";

                        return server;
                    });

                    store.set("servers", updatedServers);
                },
            },
        });

        if (!this.hasDefaultConditionSet()) {
            const current = this.store.store;

            current.conditionsSets[0].isDefault = true;

            this.store.store = current;
        }
    }

    /**
     * Saves the new settings to the config file
     * @param newSettings New settings to save
     */
    public save(newSettings: ProxySettings) {
        this.store.store = newSettings;
    }

    /**
     * Returns the store with all the ProxySettings
     * @returns The entire store from the config
     */
    public getAll(): ProxySettings {
        return this.store.store;
    }

    /**
     *
     * @param callback called when the underlying storage changes
     * @returns function to call to unsubscribe
     */
    public watchForChanges(callback: WatchStoreCallback) {
        return this.store.onDidAnyChange(callback);
    }

    private hasDefaultConditionSet() {
        return this.getAll().conditionsSets.some((set) => set.isDefault);
    }
}
