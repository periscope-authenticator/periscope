import * as React from "react";
import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Button,
    Checkbox,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    FormControlLabel,
    List,
    ListItem,
    Radio,
    Stack,
    TextField,
    Tooltip,
    Typography,
} from "@mui/material";
import { ExpandMore, WarningAmber } from "@mui/icons-material";
import { None, Option, Some } from "ts-results";
import { ConditionStore } from "../../http-condition/condition-store";
import { ImportedConditionSet } from "../../types/server";
import { lightBlue } from "@mui/material/colors";
import { makeOption } from "../../types/option";

const genInitState = () => ({ errorMessage: None, conditionImportsActions: {} });

export type ImportedConditionSetsWithConflicts = ImportedConditionSet & { hasConflict: boolean };
type ImportDialogState = { errorMessage: Option<string>; conditionImportsActions: { [id: string]: ImportAction } };
export const ImportDialog = ({
    conditionSets,
    shouldOpen,
    onImportClosed,
    willOverwrite,
}: {
    conditionSets: ImportedConditionSetsWithConflicts[];
    shouldOpen: boolean;
    onImportClosed: (conditionsToImport: ImportedConditionSet[]) => void;
    willOverwrite: (name: string) => boolean;
}) => {
    const onCancel = () => {
        onImportClosed([]);
    };

    const [actions, setActions] = React.useState<ImportDialogState>(genInitState());

    const onImportClicked = () => {
        const setsToImport = [];
        for (const set of conditionSets) {
            const action = actions.conditionImportsActions[set.id];
            if (action == null || action._type === "no-import") continue;

            if (action._type === "import-change-name") {
                if (willOverwrite(action.value)) {
                    setActions({
                        ...actions,
                        errorMessage: Some(`Condition with name ${action.value} will be overwritten`),
                    });
                    return;
                }
                setsToImport.push({ ...set, name: action.value });
                continue;
            }

            setsToImport.push(set);
        }

        onImportClosed(setsToImport);
    };

    React.useEffect(() => {
        setActions(genInitState());
    }, [shouldOpen]);

    return (
        <Dialog open={shouldOpen} onClose={onCancel} maxWidth="lg">
            <DialogTitle>Which condition sets should be imported?</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Check which Condition Sets to import. If there is a name collision you may overwrite existing or
                    change its name and import.
                </DialogContentText>
                <DialogContentText>
                    {actions.errorMessage.some && (
                        <Stack sx={{ color: "error.main" }} direction="row" alignItems="center" justifyContent="center">
                            <WarningAmber />
                            <Typography variant="body2">{actions.errorMessage.val}</Typography>
                        </Stack>
                    )}
                </DialogContentText>
                {conditionSets.map((set) => {
                    return (
                        <ConditionRow
                            key={set.id}
                            conditionSet={set}
                            importAction={actions.conditionImportsActions[set.id] ?? DoNotImport()}
                            onConditionSetChange={(action) =>
                                setActions({
                                    ...actions,
                                    conditionImportsActions: {
                                        ...actions.conditionImportsActions,
                                        [set.id]: action,
                                    },
                                })
                            }
                        />
                    );
                })}
            </DialogContent>
            <DialogActions>
                <Button onClick={onCancel}>Cancel</Button>
                <Button onClick={onImportClicked}>Import</Button>
            </DialogActions>
        </Dialog>
    );
};

const DoNotImport = makeOption<"no-import", void>("no-import");
const ImportAndOverride = makeOption<"override", void>("override");
const ImportWithName = makeOption<"import-change-name", string>("import-change-name");

type ImportAction =
    | ReturnType<typeof DoNotImport>
    | ReturnType<typeof ImportAndOverride>
    | ReturnType<typeof ImportWithName>;

const destructureImportAction = (action: ImportAction) => ({
    doNotImport: action._type === "no-import",
    importAndOverride: action._type === "override",
    importWithName: action._type === "import-change-name",
});

const ConditionRow = ({
    conditionSet,
    importAction,
    onConditionSetChange,
}: {
    conditionSet: ImportedConditionSetsWithConflicts;
    importAction: ImportAction;
    onConditionSetChange: (val: ImportAction) => void;
}) => {
    const { doNotImport, importAndOverride, importWithName } = destructureImportAction(importAction);
    const { hasConflict } = conditionSet;
    return (
        <Accordion sx={{ bgcolor: !doNotImport ? lightBlue[100] : undefined }}>
            <AccordionSummary expandIcon={<ExpandMore />}>
                <Stack direction="row" spacing={2} alignItems="center">
                    <Checkbox
                        checked={!doNotImport}
                        onClick={(event) => event.stopPropagation()}
                        onChange={(event) => {
                            onConditionSetChange(event.currentTarget.checked ? ImportAndOverride() : DoNotImport());
                            event.stopPropagation();
                        }}
                    />
                    <div style={{ minWidth: "15em" }}>
                        {importWithName ? (
                            <TextField
                                value={importAction.value}
                                onChange={(event) => {
                                    onConditionSetChange(ImportWithName(event.currentTarget.value));
                                }}
                            ></TextField>
                        ) : (
                            <Typography variant="body1" mr="auto">
                                {conditionSet.name}
                            </Typography>
                        )}
                    </div>
                    {hasConflict && (
                        <div>
                            <FormControlLabel
                                label="Overwrite Existing"
                                control={
                                    <Radio
                                        size="small"
                                        checked={importAndOverride}
                                        onClick={(event) => event.stopPropagation()}
                                        onChange={(event) => {
                                            event.stopPropagation();
                                            if (event.currentTarget.checked) {
                                                onConditionSetChange(ImportAndOverride());
                                            }
                                        }}
                                    />
                                }
                            />
                            <FormControlLabel
                                label="Change Name"
                                control={
                                    <Radio
                                        size="small"
                                        checked={importWithName}
                                        onClick={(event) => event.stopPropagation()}
                                        onChange={(event) => {
                                            event.stopPropagation();
                                            if (event.currentTarget.checked) {
                                                onConditionSetChange(ImportWithName(conditionSet.name));
                                            }
                                        }}
                                    />
                                }
                            />
                            <Tooltip title="Another set with this name already exists.">
                                <WarningAmber sx={{ color: "warning.main" }} />
                            </Tooltip>
                        </div>
                    )}
                </Stack>
            </AccordionSummary>
            <AccordionDetails>
                <Typography variant="h5">Conditions</Typography>
                <ConditionList conditionSet={conditionSet} />
            </AccordionDetails>
        </Accordion>
    );
};

const conditionViewDict = {
    header: (condition: ConditionStore) => <HeaderConditionView condition={condition} key={condition.id} />,
    "response-code": (condition: ConditionStore) => (
        <ResponseCodeConditionView condition={condition} key={condition.id} />
    ),
};

const ConditionList = ({ conditionSet }: { conditionSet: ImportedConditionSet }) => {
    return (
        <List>
            {conditionSet.conditions.map((condition) => {
                return conditionViewDict[condition.conditionSource](condition);
            })}
        </List>
    );
};

const HeaderConditionView = ({ condition }: { condition: ConditionStore }) => {
    return (
        <ListItem>
            Header {condition.headerName} {condition.headerComparator} {condition.headerValue}
        </ListItem>
    );
};

const ResponseCodeConditionView = ({ condition }: { condition: ConditionStore }) => {
    return (
        <ListItem>
            Response Code {condition.numberComparator} {condition.numberValue}
        </ListItem>
    );
};
