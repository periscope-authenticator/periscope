import * as http from "node:http";
import * as https from "node:https";

import { IncomingRequestServer, buildIncomingRequestServer } from "./incoming-request-server";
import { None, Option } from "ts-results";
import { Server, ServerStatus, StatusType } from "./types/server";
import { isBlank, trimLeadingSlash } from "./utils";

import { CookieGrabber } from "./cookie-grabber";
import EventEmitter from "node:events";
import { HeaderTransform } from "./header-transform";
import { HttpConditionManager } from "./http-condition/http-condition-manager";
import { HttpProxyInfo } from "./http-proxy-info";
import { IExtraCertStore } from "./extra-cert-store";
import { TLSCertificateManager } from "./tls-certificate-manager";
import { URL } from "node:url";
import log from "electron-log/main";
import { v4 as uuidv4 } from "uuid";

/**
 * Selected protocol for the ProxyServer
 */
type HTTPProtocol = "http" | "https";

/**
 * This interface has some of the extensions https.request has.
 * That object has no types and takes in an object so adding in the types
 */
interface HttpsOptions extends http.RequestOptions {
    ca?: string | string[] | Buffer | Buffer[];
    cert?: string | string[] | Buffer | Buffer[];
    rejectUnauthorized?: boolean;
}

export interface ServerInfo {
    name: string;
    listenPort: string;
    proxyAddress: string;
    proxyPort: string;
}

type StatusChangeCallback = (status: ServerStatus) => void;
const OK = 200;
const BAD_GATEWAY = 502;

const logger = log.scope("ProxyServer");

/**
 * Handles a proxied connection between a client and a remote host to be able to inject cookies
 */
export class ProxyServer extends EventEmitter {
    private server: IncomingRequestServer;
    // These are definitely initialized in the fillFromServerInfo but Typescript is not smart enough to go into that function
    private _name!: string;
    private listenPort!: number;
    private proxyAddress!: string;
    private proxyPort!: number;
    private protocol!: HTTPProtocol;
    private certHolder: IExtraCertStore;
    private conditionSet!: string;
    private requestPath?: string;
    private currentStatus: StatusType = "stopped";
    private statusMessage?: string;
    private cookieGrabber: CookieGrabber;
    private useProxy!: boolean;
    private useTLS!: boolean;
    private proxyData: Option<HttpProxyInfo> = None;

    public readonly id: string;

    /**
     * Creates a ProxyServer instance. This method only registers events but does not start listening for connections
     * @param serverInfo config of the server
     * @param certHolder holds extra certs needed for internal proxies
     */
    constructor(
        serverInfo: Server,
        certHolder: IExtraCertStore,
        private conditionManager: HttpConditionManager,
        private headerTransform: HeaderTransform,
        private certificateManager: TLSCertificateManager,
        statusHandler: (status: StatusType, message?: string) => void
    ) {
        super();

        this.on('status-changed', statusHandler);

        this.id = serverInfo.id;
        this.certHolder = certHolder;

        this.fillFromServerInfo(serverInfo);
        this.cookieGrabber = new CookieGrabber(this.buildUrl());

        this.server = buildIncomingRequestServer(this.listenPort, this.useTLS, this.certificateManager, (error) => this.changeStatus('error', error));
        this.setupServer();
    }

    /**
     * the name of the server
     */
    public get name(): string {
        return this._name;
    }

    public get serverInfo(): ServerInfo {
        return {
            name: this.name,
            listenPort: this.listenPort.toString(),
            proxyAddress: this.proxyAddress,
            proxyPort: this.proxyPort.toString(),
        };
    }

    /**
     * Starts listening asynchronously on the port provided for new connections
     */
    public startListening() {
        this.server.start();
    }

    /**
     * Shuts down the server and closes all existing connections
     */
    public shutdown() {
        this.server.stop();
    }

    /**
     * Shuts down the server. Promise will resolve once the server is closed
     */
    public shutdownAsync(): Promise<void> {
        return this.shutdownAsync();
    }

    /**
     * Starts listening on the server. Will resolve once the server starts listening
     */
    public startListeningAsync(): Promise<void> {
        return this.startListeningAsync();
    }

    /**
     * Restarts the server
     */
    public async restart(): Promise<void> {
        await this.shutdownAsync();
        await this.startListeningAsync();
    }

    /**
     * Updates the settings of the server. Restarts if necessary
     * @param newSettings Settings to apply
     */
    public async updateSettings(newSettings: Server, proxySettings: Option<HttpProxyInfo>): Promise<void> {
        let needsRestart = false;

        this.proxyData = proxySettings;

        if (this.listenPort !== parseInt(newSettings.localPort)) {
            needsRestart = true;
        }

        if (this.useTLS !== newSettings.useTLS) {
            this.cleanupServer();
            await this.server.stopAsync();

            this.server = buildIncomingRequestServer(this.listenPort, newSettings.useTLS, this.certificateManager, (error) => this.changeStatus('error', error));
            this.setupServer();
            try {
                await this.server.startAsync();
            } catch {
                log.error(`Failed to start server ${this.name}`);
            }
        }

        this.fillFromServerInfo(newSettings);
        this.cookieGrabber = new CookieGrabber(this.buildUrl());

        if (needsRestart) {
            this.server.updateServerPort(this.listenPort);
        }
    }

    public onStatusChange(callback: StatusChangeCallback) {
        this.on("status-changed", callback);
    }

    public status(): ServerStatus {
        return {
            server: {
                id: this.id,
                name: this._name,
            },
            status: this.currentStatus,
            message: this.statusMessage,
        };
    }

    private onChangeStatus = (status: StatusType) => {
        this.changeStatus(status);
    };

    private onServerRequest = (req: http.IncomingMessage, res: http.ServerResponse) => this.onRequest(req, res);

    private setupServer() {
        this.server.on('status-update', this.onChangeStatus);
        this.server.on('request', this.onServerRequest);
    }

    private cleanupServer() {
        this.server.off('status-update', this.onChangeStatus);
        this.server.off('request', this.onServerRequest);
        this.server.cleanupServer();
    }

    private changeStatus(newStatus: StatusType, message?: string) {
        logger.debug("Changing the status for", this.name, newStatus);
        this.currentStatus = newStatus;
        this.statusMessage = message;
        this.emit("status-changed", this.status());
    }

    private fillFromServerInfo(serverInfo: Server) {
        this._name = serverInfo.name;

        const url = new URL(serverInfo.url);
        this.proxyAddress = url.hostname;
        this.setProxyPort(url);
        this.useTLS = serverInfo.useTLS;

        this.setProtocol(url);
        this.requestPath = serverInfo.returnPath == null ? undefined : trimLeadingSlash(serverInfo.returnPath);

        const port = parseInt(serverInfo.localPort);
        if (Number.isNaN(port)) {
            throw new TypeError("Local Port cannot be converted to a number");
        }

        this.listenPort = port;
        this.useProxy = serverInfo.useProxy;

        this.conditionSet = serverInfo.conditionSet;
        this.headerTransform.newServerInfo(this.serverInfo);
    }

    /**
     * Extracts the port from the URL
     * @param url Url to pull port from
     */
    private setProxyPort(url: URL) {
        // The port property is only set if it is a non-default port. Otherwise it is an empty string
        if (!isBlank(url.port)) {
            this.proxyPort = parseInt(url.port);
            return;
        }

        switch (url.protocol) {
            case "https:":
                this.proxyPort = 443;
                break;
            case "http:":
                this.proxyPort = 80;
                break;
            default:
                throw new TypeError("Unknown protocol");
        }
    }

    /**
     * Sets the current protocol based on the URL
     * @param url Url to extract protocol from
     */
    private setProtocol(url: URL) {
        if (url.protocol === "https:" || url.protocol === "http:") {
            this.protocol = url.protocol.slice(0, -1) as HTTPProtocol;
            return;
        }

        throw new TypeError("URL has an unknown protocol");
    }

    /**
     * Handles incoming request and proxies them to the server
     * @param request Incoming request from the local client
     * @param response Outgoing response back to the client
     */
    private onRequest(request: http.IncomingMessage, response: http.ServerResponse, requestId: string = uuidv4()) {
        logger.info(`ID: [${requestId}] METHOD: ${request.method} URL: ${request.url}`);
        logger.silly(request.rawHeaders);

        try {
            this.passRequest(request, response, requestId);
        } catch (e) {
            let message: string;
            let name = "Unknown Error";
            if (e instanceof Error) {
                message = e.message;
                name = e.name;
            } else {
                message = String(e);
            }
            ProxyServer.buildErrorHandler(response, requestId)({ message, name });
        }
    }

    /**
     * Builds an error handling function with the passed in response
     * @param response Response to use in the error handler to respond
     * @returns Error handling function
     */
    private static buildErrorHandler = (response: http.ServerResponse, requestId: string) => (err: Error) => {
        logger.error(`ID: [${requestId}] ERROR ${err.message}, ${err.stack}`);
        if (!response.headersSent) {
            response.writeHead(BAD_GATEWAY, http.STATUS_CODES[BAD_GATEWAY]).end(`Error in proxy: ${err.message}`);
        }
    };

    /**
     * Pass the request to the remote server
     * @param request Request to pass to remote server
     * @param response Response back to local client
     */
    private passRequest(request: http.IncomingMessage, response: http.ServerResponse, requestId: string) {
        logger.silly("in passRequest");
        const requestOptions = this.incomingToRequestOptions(request);
        const localErrorHandler = ProxyServer.buildErrorHandler(response, requestId);

        this.createRequest(requestOptions, (serverResponse) =>
            this.pipeResponseToClient(serverResponse, response, request, requestId),
        )
            .then((newRequest) => {
                logger.silly("new request created");

                newRequest.on("timeout", () => {
                    const GATEWAY_TIMEOUT = 504;
                    logger.warn(`Request ${requestId} for proxy ${this.proxyAddress} timed out`);
                    response
                        .writeHead(GATEWAY_TIMEOUT, http.STATUS_CODES[GATEWAY_TIMEOUT])
                        .end(`Connection timeout to ${this.proxyAddress}`);
                    newRequest.destroy();
                });

                // Sending the body of the original request to the new request
                request.pipe(newRequest, { end: true });

                request.on("error", localErrorHandler);

                newRequest.on("error", localErrorHandler);
            })
            .catch(localErrorHandler);
    }

    private createRequest(
        options: HttpsOptions,
        onResponse: (res: http.IncomingMessage) => void,
    ): Promise<http.ClientRequest> {
        if (!this.useProxy || this.proxyData.none) {
            return Promise.resolve(this.createInnerRequest(options, onResponse));
        }

        return new Promise((resolve, reject) => {
            if (this.proxyData.none) {
                reject(new Error("Use Proxy set but no Proxy Data"));
                return;
            }
            logger.info("Making a proxy connection");
            const authData = this.proxyData.val.authenticationHeader();

            const headers: http.OutgoingHttpHeaders = {};
            if (authData.some) {
                headers["Proxy-Authorization"] = authData.val;
            }

            http.request({
                host: this.proxyData.val.host(),
                port: this.proxyData.val.port(),
                method: "CONNECT",
                path: `${options.host}:${options.port}`,
                headers,
            })
                .on("connect", (res, socket) => {
                    if (res.statusCode === OK) {
                        logger.info("Successfully connected to proxy. Creating a new connection");
                        const agent = new https.Agent({ socket });
                        options.agent = agent;
                        resolve(this.createInnerRequest(options, onResponse));
                        return;
                    }
                    reject(new Error("Failed to connect to proxy"));
                })
                .on("error", (err) => {
                    reject(err);
                })
                .end();
        });
    }

    /**
     * Creates a request using the options provided with the onResponse. This will not execute the response, end() is still needed to be called.
     * @param options Options to use in the request
     * @param onResponse Callback function when the response occurs for the response
     * @returns Request from options
     */
    private createInnerRequest(
        options: HttpsOptions,
        onResponse: (res: http.IncomingMessage) => void,
    ): http.ClientRequest {
        if (this.protocol === "http") {
            return http.request(options, onResponse);
        }

        if (this.certHolder.hasExtraCerts()) {
            options.ca = this.certHolder.extraCerts();
        }

        return https.request(options, onResponse);
    }

    /**
     * Writes the contents of the remote response to the local client
     * @param serverResponse Response from the remote server
     * @param clientResponse Response for the local client to write to
     */
    private pipeResponseToClient(
        serverResponse: http.IncomingMessage,
        clientResponse: http.ServerResponse,
        originalRequest: http.IncomingMessage,
        requestId: string,
    ) {
        const shouldRetrieveCookies = this.conditionManager
            .getConditions(this.conditionSet)
            .every((condition) => condition.shouldRetrieveCookies(serverResponse));

        if (shouldRetrieveCookies) {
            logger.debug("We need more cookies for request", requestId);
            this.cookieGrabber
                .grabCookie()
                .then((cookies) => {
                    this.headerTransform.onNewCookies(cookies);
                    // TODO: Need to have a redirect limit of some kind
                    this.onRequest(originalRequest, clientResponse, requestId);
                })
                .catch(ProxyServer.buildErrorHandler(clientResponse, requestId));
            return;
        }

        const updatedHeaders = this.headerTransform.transformHeaderForClientResponse(
            originalRequest.headers,
            serverResponse.headers,
        );

        logger.info(`ID: [${requestId}] Write Header: ${serverResponse.statusCode}, ${serverResponse.statusMessage}`);
        clientResponse.writeHead(
            serverResponse.statusCode ?? BAD_GATEWAY,
            serverResponse.statusMessage,
            updatedHeaders,
        );
        logger.silly("Server pipe to client");
        serverResponse.pipe(clientResponse);
    }

    /**
     * Builds a new request options based on the incoming request modified for the remote server
     * @param request Original request from local client
     * @returns New options to build the request from
     */
    private incomingToRequestOptions(request: http.IncomingMessage): http.RequestOptions {
        const modifiedHeaders = this.headerTransform.transformHeadersForRemoteRequest(request.headers);

        const options: http.RequestOptions = {
            host: this.proxyAddress,
            port: this.proxyPort,
            headers: modifiedHeaders,
            method: request.method,
            path: request.url,
        };

        return options;
    }

    private buildUrl(): string {
        const uri = new URL(`${this.protocol}://${this.proxyAddress}`);
        uri.port = this.proxyPort.toFixed();
        if (this.requestPath != null) {
            uri.pathname = this.requestPath;
        }

        return uri.href;
    }

    private static readBody(request: http.IncomingMessage): Promise<string> {
        let body = "";

        request.on("data", (chunk) => (body += chunk));

        const promise = new Promise<string>((resolve, reject) => {
            request.on("end", () => resolve(body));
            request.on("error", (err) => reject(err));
        });

        return promise;
    }
}
