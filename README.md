# Periscope

Periscope allows command line applications to interact with web applications that require authentication with a browser with cookies. It accomplishes this by proxying requests with an electron app. When the application detects that authentication is required, it will open a new window with a web browser to retrieve the necessary cookies. The backend will then use those cookies when making future requests.

A more detailed wiki can be found [here](https://periscope-authenticator.gitlab.io/periscope-docs/)

## Usage

### Server Setup

To be able to proxy to a server they need to be inputted into the Server Settings pane

- Name of the server
- URL Address of the server
- Local port to connect to as a proxy

The current configuration is also applied automatically once the settings are saved.

### Conditions Setup

To be able to detect when a browser window needs to be opened for authentication, the conditions for when that occurs need to be set.

On the General Settings tab, new conditions can be added to detect when the browser needs to be opened. When all conditions are true for a response from the server, a browser window will be opened to the Server Address setup in the Server Settings.

Currently, there are only a couple of options for data sources for conditions but more are planned to be added in the future:

- HTTP Status Code
- HTTP Headers
    - Content Type
    - Location
    - Server


## Development Setup

The application is based on Node v18. We have an .nvmrc setup with our current Node version.

We also use Yarn as a package manager which needs to be install if not already

```
npm install -g yarn
```

Install development packages

```
yarn
```

If your network is behind an HTTP proxy you may need to set the following environment variables

```
NODE_EXTRA_CA_CERTS=<cafile>
ELECTRON_GET_USE_HTTP=true
```

After installation, to run the application in development mode:

```
yarn start
```

For building executables see: https://periscope-authenticator.gitlab.io/periscope-docs/dev-guide/#building-executables
