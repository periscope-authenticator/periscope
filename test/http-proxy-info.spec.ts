import { None, Some } from "ts-results";
import { suite, test } from "mocha";
import { HttpProxyInfoImpl } from "../src/http-proxy-info";
import { assert } from "chai";

suite("HttpProxyInfoImpl", function () {
    suite("host", function () {
        test("should return host", function () {
            const url = new URL("http://example.com");
            const data = new HttpProxyInfoImpl(url);
            assert.equal(data.host(), "example.com");
        });
    });

    suite("port", function () {
        test("http no port should be 80", function () {
            const url = new URL("http://example.com");
            const data = new HttpProxyInfoImpl(url);
            assert.equal(data.port(), "80");
        });

        test("https no port should be 443", function () {
            const url = new URL("https://example.com");
            const data = new HttpProxyInfoImpl(url);
            assert.equal(data.port(), "443");
        });

        test("port should return correct port", function () {
            const url = new URL("http://example.com:8000");
            const data = new HttpProxyInfoImpl(url);
            assert.equal(data.port(), "8000");
        });
    });

    suite("authenticationHeader", function () {
        test("no auth returns None", function () {
            const url = new URL("http://example.com");
            const data = new HttpProxyInfoImpl(url);
            assert.equal(data.authenticationHeader(), None);
        });

        test("auth returns Some", function () {
            const url = new URL("http://user:pass@example.com");
            const data = new HttpProxyInfoImpl(url);
            assert.instanceOf(data.authenticationHeader(), Some);
        });

        test("auth has Basic header", function () {
            const url = new URL("http://user:pass@example.com");
            const data = new HttpProxyInfoImpl(url);
            const header = data.authenticationHeader();
            assert.isTrue(header.some);
            assert.isTrue(header.unwrap().includes("Basic"));
        });
    });
});
