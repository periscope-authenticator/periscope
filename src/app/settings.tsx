import * as React from "react";

import {
    AppBar,
    Box,
    Button,
    FormControl,
    FormControlLabel,
    FormGroup,
    IconButton,
    InputLabel,
    MenuItem,
    Select,
    SelectChangeEvent,
    Stack,
    Switch,
    Tab,
    Tabs,
    TextField,
    Toolbar,
    Tooltip,
    Typography,
} from "@mui/material";
import { ConditionSet, ProxySettings, Server, ServerStatus, ServerStatusCallback } from "../types/server";
import { GeneralSettings, ProxyStateBase } from "./settings/general-settings";
import { ValidateReturn, ValidatedField } from "./validated-field";
import { groupArrayBy, isBlank, useDidUpdateEffect } from "../utils";

import { AppInfoDialog } from "./app-info";
import { BadgedLabel } from "./badged-label";
import { ChangeEvent } from "./events";
import { ConditionStore } from "../http-condition/condition-store";
import { ConditionTab } from "./condition-settings";
import { InfoOutlined } from "@mui/icons-material";
import { NotificationContext } from "./notification-context";
import { NotifyPanel } from "./notify-panel";
import SaveIcon from "@mui/icons-material/Save";
import { StatusPanel } from "./status-panel";
import { TabPanel } from "./tab-panel";
import log from "electron-log/renderer";
import { v4 as uuidv4 } from "uuid";

interface SettingsState {
    proxyUrl: string;
    servers: Map<string, Server>;
    conditionSets: ConditionSet[];
    serverIndex: number;
    serverStatuses: ServerStatus[];
    openInfo: boolean;
    tlsCertificate?: string;
    tlsKey?: string;
}

export interface ActionValue<V, Event = ChangeEvent> {
    value: V;
    onChange: (e: Event) => void;
}

interface ServerPropBase {
    id: string;
    name: ActionValue<string>;
    url: ActionValue<string>;
    localPort: ActionValue<string>;
    useProxy: ActionValue<boolean>;
    useTLS: ActionValue<boolean>;
    conditionSet: ActionValue<string, SelectChangeEvent>;
    requestPath: ActionValue<string | undefined>;
    onDelete: () => void;
    conditionSets: ConditionSet[];
}

interface ServerProp extends ServerPropBase {
    onValidated: OnValidated;
}

interface ServersPanelPropBase {
    servers: ServerPropBase[];
    conditionSets: ConditionSet[];
    onAddNewServer: () => void;
    serverIndex: number;
    serverIndexChanged: (index: number) => void;
}

interface ServerPanelProp extends ServersPanelPropBase {
    onValidated: OnValidated;
}

interface MainPanelProps extends ServersPanelPropBase, ProxyStateBase {
    onValidated: OnValidated;
    conditionSets: ConditionSet[];
    onConditionsChanged: (conditionSets: ConditionSet[]) => void;
    onAddNewCondition: () => void;
    serverStatuses: ServerStatus[];
}

export type OnValidated = (isValid: boolean) => void;

interface TopBarProps {
    onSave: () => void;
    onInfo: () => void;
}

function TopBar(props: TopBarProps) {
    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        Settings
                    </Typography>
                    <IconButton aria-label="info" color="inherit" onClick={props.onInfo}>
                        <InfoOutlined />
                    </IconButton>
                    <IconButton aria-label="save" color="inherit" onClick={props.onSave}>
                        <SaveIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>
        </Box>
    );
}

export const defaultValidator = (input: HTMLInputElement): ValidateReturn => {
    return {
        isValid: input.checkValidity(),
    };
};

interface ServerValidation {
    nameIsValid: boolean;
    hostIsValid: boolean;
    portIsValid: boolean;
}

function ServerSetting(props: ServerProp) {
    const [validations, setValidations] = React.useState<ServerValidation>({
        nameIsValid: true,
        hostIsValid: true,
        portIsValid: true,
    });
    const { onValidated, name, url, localPort, useProxy, onDelete, conditionSets, conditionSet, requestPath, useTLS } =
        props;

    useDidUpdateEffect(() => {
        onValidated(validations.nameIsValid && validations.hostIsValid && validations.portIsValid);
    }, [validations, onValidated]);

    const LOWEST_PORT = 1024;
    const HIGHEST_PORT = 65535;

    const portValidator = React.useCallback((input: HTMLInputElement): ValidateReturn => {
        const value = input.value;
        const numValue = Number.parseInt(value);
        const isValid = !Number.isNaN(numValue) && numValue >= LOWEST_PORT && numValue <= HIGHEST_PORT;

        setValidations((current) => {
            return { ...current, portIsValid: isValid };
        });

        return {
            isValid: isValid,
        };
    }, []);

    const nameValidator = React.useCallback((input: HTMLInputElement): ValidateReturn => {
        const validation = defaultValidator(input);

        setValidations((current) => {
            return { ...current, nameIsValid: validation.isValid };
        });

        return validation;
    }, []);

    const urlValidator = React.useCallback((input: HTMLInputElement): ValidateReturn => {
        const validation = defaultValidator(input);

        setValidations((current) => {
            return { ...current, hostIsValid: validation.isValid };
        });

        return validation;
    }, []);

    const selectOptions = React.useMemo(() => {
        return [{ id: "default", name: "Use Default" }, ...conditionSets].map((set) => (
            <MenuItem key={set.id} value={set.id}>
                {set.name}
            </MenuItem>
        ));
    }, [conditionSets]);

    return (
        <Stack spacing={2}>
            <ValidatedField
                id="serverName"
                label="Name"
                required
                onChange={name.onChange}
                value={name.value}
                validateCallback={nameValidator}
            />
            <ValidatedField
                id="serverUrl"
                label="https://host/"
                required
                onChange={url.onChange}
                value={url.value}
                helperText="Server Address"
                type="url"
                validateCallback={urlValidator}
            />
            <ValidatedField
                id="localPort"
                label={`${LOWEST_PORT}-${HIGHEST_PORT}`}
                required
                onChange={localPort.onChange}
                value={localPort.value}
                helperText="Local Port"
                type="number"
                inputProps={{ min: 0 }}
                validateCallback={portValidator}
            />
            <FormGroup>
                <FormControlLabel
                    control={
                        <Switch
                            id="useProxy"
                            onChange={useProxy.onChange}
                            value={useProxy.value}
                            checked={useProxy.value}
                        />
                    }
                    label="Use Proxy"
                />
            </FormGroup>
            <FormGroup>
                <FormControlLabel
                    control={
                        <Switch id="useTLS" onChange={useTLS.onChange} value={useTLS.value} checked={useTLS.value} />
                    }
                    label="Use TLS"
                />
            </FormGroup>
            <FormControl>
                <InputLabel id="server-condition-select-label">Condition Set</InputLabel>
                <Select
                    labelId="server-condition-select-label"
                    id="server-select"
                    label="Condition Set"
                    onChange={conditionSet.onChange}
                    value={conditionSet.value}
                >
                    {selectOptions}
                </Select>
            </FormControl>
            <FormControl>
                <Tooltip title="If your authentication redirects you to a different path, (e.g. http://example.com/auth instead of http://example.com) put the path (e.g. /auth) here">
                    <TextField
                        id="redirect-landing-path"
                        label="e.g. /auth"
                        helperText="Redirect Landing Path"
                        value={requestPath.value}
                        onChange={requestPath.onChange}
                    />
                </Tooltip>
            </FormControl>
            <Box>
                <Button variant="contained" color="error" onClick={onDelete} sx={{ float: "left" }}>
                    Delete Server
                </Button>
            </Box>
        </Stack>
    );
}

function ServerPanel(props: ServerPanelProp) {
    const { onValidated, servers, onAddNewServer, serverIndex, serverIndexChanged } = props;
    const handleTabChange = React.useCallback(
        (_: React.SyntheticEvent<Element>, newIndex: number) => {
            serverIndexChanged(newIndex);
        },
        [serverIndexChanged],
    );

    const [serverValidations, setServerValidations] = React.useState<boolean[]>(() => props.servers.map(() => true));

    React.useEffect(() => {
        const isValid = serverValidations.reduce((prev, curr) => {
            return prev && curr;
        }, true);
        onValidated(isValid);
    }, [serverValidations, onValidated]);

    const serverTabs = servers.map((server) => {
        return <Tab key={server.id} label={server.name.value} />;
    });

    const buildServerValidationCallback = (index: number) => (isValid: boolean) => {
        setServerValidations((current) => {
            const newValidations = [...current];
            newValidations[index] = isValid;
            return newValidations;
        });
    };

    // This line is saving off the callbacks unless the server array changes
    // If not the callback would cause the callback itself to change and continuously trigger itself cause state setting issues
    const serverValidationCallbacks = React.useMemo<OnValidated[]>(() => {
        return servers.map((v, idx) => buildServerValidationCallback(idx));
    }, [servers]);

    const serverItems = servers.map((server, idx) => {
        return (
            <TabPanel key={server.id} selectedIndex={serverIndex} index={idx} sx={{ flexGrow: 1, display: "flex" }}>
                <ServerSetting {...server} onValidated={serverValidationCallbacks[idx]} />
            </TabPanel>
        );
    });

    return (
        <Stack>
            <Box>
                <Button onClick={() => onAddNewServer()} variant="text" sx={{ float: "right" }}>
                    Add New Server
                </Button>
            </Box>
            <Box sx={{ flexGrow: 1, bgcolor: "background.paper", display: "flex" }}>
                <Tabs
                    value={serverIndex}
                    onChange={handleTabChange}
                    orientation="vertical"
                    sx={{ borderRight: 1, borderColor: "divider" }}
                >
                    {serverTabs}
                </Tabs>
                <Stack spacing="2" sx={{ flexGrow: 1, display: "flex" }}>
                    {serverItems}
                </Stack>
            </Box>
        </Stack>
    );
}

interface MainValidatedState {
    isGeneralValidated: boolean;
    isServerValidated: boolean;
}

function MainPanel(props: MainPanelProps) {
    const [tabIndex, setTabIndex] = React.useState(1);
    const [validatedState, setValidatedState] = React.useState<MainValidatedState>({
        isGeneralValidated: true,
        isServerValidated: true,
    });
    const handleTabChange = (event: React.SyntheticEvent, newIndex: number) => {
        setTabIndex(newIndex);
    };

    const onGeneralValidated = React.useCallback((isValid: boolean) => {
        setValidatedState((current) => {
            return { ...current, isGeneralValidated: isValid };
        });
    }, []);

    const onServerValidated = React.useCallback((isValid: boolean) => {
        setValidatedState((current) => {
            return { ...current, isServerValidated: isValid };
        });
    }, []);

    const {
        onValidated,
        servers,
        conditionSets,
        onConditionsChanged,
        onAddNewServer,
        serverIndex,
        serverIndexChanged,
        onAddNewCondition,
        serverStatuses,
    } = props;

    React.useEffect(() => {
        onValidated(validatedState.isGeneralValidated && validatedState.isServerValidated);
    }, [validatedState, onValidated]);

    const generalLabel = (
        <BadgedLabel color="error" hasBadge={!validatedState.isGeneralValidated}>
            {"General Settings"}
        </BadgedLabel>
    );

    const serversLabel = (
        <BadgedLabel color="error" hasBadge={!validatedState.isServerValidated}>
            {"Server Settings"}
        </BadgedLabel>
    );

    const tabs: [React.ReactNode, () => JSX.Element][] = [
        [generalLabel, () => <GeneralSettings {...props} onValidated={onGeneralValidated} />],
        [
            serversLabel,
            () => (
                <ServerPanel
                    servers={servers}
                    onValidated={onServerValidated}
                    onAddNewServer={onAddNewServer}
                    serverIndex={serverIndex}
                    serverIndexChanged={serverIndexChanged}
                    conditionSets={conditionSets}
                />
            ),
        ],
        [
            "Redirect Conditions",
            () => (
                <ConditionTab
                    conditionSets={conditionSets}
                    onChange={onConditionsChanged}
                    onAddNew={onAddNewCondition}
                />
            ),
        ],
        ["Server Status", () => <StatusPanel statuses={serverStatuses} />],
    ];

    return (
        <Stack sx={{ flexGrow: 1, bgcolor: "background.paper", display: "flex" }}>
            <Tabs value={tabIndex} onChange={handleTabChange} sx={{ borderRight: 1, borderColor: "divider" }}>
                {tabs.map(([tabName], index) => {
                    return <Tab label={tabName} key={index} />;
                })}
                ;
            </Tabs>
            <NotifyPanel />
            <Stack spacing="2" sx={{ flexGrow: 1, display: "flex" }}>
                {tabs.map(([_, tabFunc], index) => {
                    return (
                        <TabPanel
                            selectedIndex={tabIndex}
                            key={index}
                            index={index}
                            sx={{ flexGrow: 1, display: "flex" }}
                        >
                            {tabFunc()}
                        </TabPanel>
                    );
                })}
            </Stack>
        </Stack>
    );
}

interface SettingsProps {
    initialSettings: ProxySettings;
    initialStatus?: ServerStatus[];
}

const emptyServer: Omit<Server, "id"> = {
    name: "New Server",
    url: "",
    localPort: "",
    useProxy: false,
    conditionSet: "default",
    useTLS: false,
} as const;

const emptyConditionSet = {
    name: "Conditions",
    conditions: [] as ConditionStore[],
};

const buildEmptyServer = (): Server => {
    return { id: uuidv4(), ...emptyServer };
};

const buildEmptyConditionSet = (isDefault: boolean): ConditionSet => {
    return { id: uuidv4(), ...emptyConditionSet, isDefault };
};

class Settings extends React.Component<SettingsProps, SettingsState> {
    static contextType = NotificationContext;
    declare context: React.ContextType<typeof NotificationContext>;
    private isValid = true;
    private onStatusChanged?: ServerStatusCallback;
    private onAboutMenu?: () => void;

    constructor(props: SettingsProps) {
        super(props);

        const initialServers = props.initialSettings.servers ?? [];

        if (initialServers.length < 1) {
            initialServers.push(buildEmptyServer());
        }

        const initialSets = props.initialSettings.conditionsSets ?? [];
        if (initialSets.length < 1) {
            initialSets.push(buildEmptyConditionSet(true));
        }

        const servers = new Map<string, Server>(initialServers.map((server) => [server.id, server]));

        this.state = {
            proxyUrl: props.initialSettings.proxyUrl,
            tlsKey: props.initialSettings.tlsKeyPath,
            tlsCertificate: props.initialSettings.tlsCertificatePath,
            servers: servers,
            conditionSets: initialSets,
            serverIndex: 0,
            serverStatuses: props.initialStatus ?? [],
            openInfo: false,
        };
    }

    componentDidMount() {
        this.onStatusChanged = (_, serverStatuses: ServerStatus[]) => {
            this.setState({ serverStatuses: serverStatuses });
        };

        api.onStatusChange(this.onStatusChanged);
        api.emitUiLoaded().catch((err) => {
            this.context.notify(`Failed to notify UI loading: ${String(err)}`);
        });
        this.onAboutMenu = () => {
            this.setState({ openInfo: true });
        };
        api.onShowAboutMenu(this.onAboutMenu);
    }

    editServer = (serverID: string, callback: (server: Server) => Server) => {
        const servers = this.state.servers;
        let server = servers.get(serverID);
        if (server == null) {
            log.error("Failed to find server to edit");
            return;
        }

        server = callback(server);
        servers.set(serverID, server);

        this.setState({ servers: servers });
    };

    handleProxyUrlChange = (ev: ChangeEvent) => {
        this.setState({ proxyUrl: ev.target.value });
    };

    handleServerNameChange = (serverID: string) => (event: ChangeEvent) => {
        this.editServer(serverID, (server) => {
            server.name = event.target.value;
            return server;
        });
    };

    handleServerUrlChange = (serverID: string) => (event: ChangeEvent) => {
        this.editServer(serverID, (server) => {
            server.url = event.target.value;
            return server;
        });
    };

    handleLocalPortChange = (serverID: string) => (event: ChangeEvent) => {
        this.editServer(serverID, (server) => {
            server.localPort = event.target.value;
            return server;
        });
    };

    handleUseProxyChange = (serverID: string) => (event: ChangeEvent) => {
        this.editServer(serverID, (server) => {
            // We know it's not null because it comes from an HTMLInputElement which always has a checked property
            server.useProxy = event.target.checked!;
            return server;
        });
    };

    handleUseTLSChange = (serverID: string) => (event: ChangeEvent) => {
        this.editServer(serverID, (server) => {
            // We know it's not null because it comes from an HTMLInputElement which always has a checked property
            server.useTLS = event.target.checked!;
            return server;
        });
    };

    handleSelectedConditionSetChange = (serverID: string) => (event: SelectChangeEvent) => {
        this.editServer(serverID, (server) => {
            server.conditionSet = event.target.value;
            return server;
        });
    };

    handleRequestPathChange = (serverId: string) => (event: ChangeEvent) => {
        this.editServer(serverId, (server) => {
            server.returnPath = event.target.value;
            return server;
        });
    };

    handleServerDelete = (serverID: string) => {
        const servers = this.state.servers;
        if (servers.size === 1) {
            this.context.notify("Cannot remove the last Server", "error");
            return;
        }
        const deletedServer = servers.get(serverID);
        servers.delete(serverID);
        this.context.notify(`Server "${deletedServer?.name}" was deleted`);
        this.setState({ servers, serverIndex: 0 });
    };

    handleConditionsChanged = (conditions: ConditionSet[]) => {
        this.setState({
            ...this.state,
            conditionSets: this.validateConditions(conditions),
        });
    };

    validateConditions = (conditions: ConditionSet[]) => {
        // Validate conditions is not empty
        const newConditions = [...conditions];
        if (newConditions.length === 0) {
            newConditions.push(buildEmptyConditionSet(true));
        }

        // Validate at least one condition is the default
        if (!newConditions.some((set) => set.isDefault)) {
            newConditions[0].isDefault = true;
        }

        return newConditions;
    };

    addNewCondition = () => {
        this.handleConditionsChanged([
            ...this.state.conditionSets,
            buildEmptyConditionSet(!this.hasAnyDefaultCondition()),
        ]);
    };

    hasAnyDefaultCondition = () => {
        return this.state.conditionSets.some((set) => set.isDefault);
    };

    serversAsArray(): Server[] {
        return Array.from(this.state.servers, ([, server]) => server);
    }

    toProxySettings = (): ProxySettings => {
        const servers = this.serversAsArray();

        return {
            proxyUrl: this.state.proxyUrl,
            servers: servers,
            conditionsSets: this.state.conditionSets,
            tlsKeyPath: this.state.tlsKey,
            tlsCertificatePath: this.state.tlsCertificate,
        };
    };

    onSaveButton = () => {
        if (!this.isValid || !this.validatePorts() || !this.validateProxySettings() || !this.validateTLSSettings()) {
            this.context.notify("Could not save, config invalid", "error");
            return;
        }

        this.context.notify("Saving Settings", "info");
        api.saveSettings(this.toProxySettings())
            .then(() => {
                this.context.notify("Settings Saved", "success");
            })
            .catch((err) => {
                log.error(`Error while saving ${String(err)}`);
                this.context.notify(`Failed to save ${String(err)}`, "error");
            });
    };

    onInfoButton = () => {
        this.setState({ ...this.state, openInfo: true });
    };

    onValidatedChanged = (isValidated: boolean) => {
        this.isValid = isValidated;
    };

    validatePorts() {
        const servers = this.serversAsArray();

        const portMapping = groupArrayBy(servers, (server) => server.localPort);

        let isValid = true;

        portMapping.forEach((arr, port) => {
            if (arr.length > 1) {
                isValid = false;
                const serverNames = arr.map((server) => server.name).join(", ");
                this.context.notify(`Port ${port} is mapped on more than one server: ${serverNames}`, "warning");
            }
        });

        return isValid;
    }

    validateProxySettings() {
        // Only need to check if the proxy is unset
        if (!isBlank(this.state.proxyUrl)) {
            return true;
        }

        const servers = this.serversAsArray();
        const badServers = servers.filter((server) => server.useProxy);

        if (!isBlank(badServers)) {
            const serverNames = badServers.map((server) => server.name).join(", ");

            this.context.notify(`These servers (${serverNames}) are set to use proxy but no proxy is set`, "warning");
            return false;
        }

        return true;
    }

    noTLSCerts() {
        return isBlank(this.state.tlsKey) && isBlank(this.state.tlsCertificate);
    }

    validateTLSSettings() {
        if (this.noTLSCerts()) {
            const tlsServers = this.serversAsArray().filter((server) => server.useTLS);
            if (tlsServers.length > 0) {
                this.context.notify(
                    `These servers (${tlsServers.map((s) => s.name).join(", ")}) are set to use TLS but no cert and key are set`,
                    "warning",
                );
                return false;
            }
            return true;
        }
        if (isBlank(this.state.tlsCertificate) && !isBlank(this.state.tlsKey)) {
            this.context.notify("TLS Key is set but no TLS Certificate is provided", "warning");
            return false;
        }
        if (isBlank(this.state.tlsKey) && !isBlank(this.state.tlsCertificate)) {
            this.context.notify("TLS Certificate is set but no TLS Key is set", "warning");
            return false;
        }

        return true;
    }

    addServer() {
        const servers = this.state.servers;
        const newServer = buildEmptyServer();
        servers.set(newServer.id, newServer);
        this.setState({ servers, serverIndex: servers.size - 1 });
    }

    serverIndexChanged(index: number) {
        this.setState({ serverIndex: index });
    }

    handleTlsChange(field: "tlsKey" | "tlsCertificate"): (event: ChangeEvent) => void {
        return (event) => {
            const value = event.target.value;
            const newValue = isBlank(value) ? undefined : value;

            const newState = { ...this.state };
            newState[field] = newValue;
            this.setState(newState);
        };
    }

    render() {
        const servers: ServerPropBase[] = Array.from(this.state.servers, ([id, server]) => {
            return {
                id: id,
                name: {
                    onChange: this.handleServerNameChange(id),
                    value: server.name,
                },
                url: {
                    onChange: this.handleServerUrlChange(id),
                    value: server.url,
                },
                localPort: {
                    onChange: this.handleLocalPortChange(id),
                    value: server.localPort,
                },
                useProxy: {
                    onChange: this.handleUseProxyChange(id),
                    value: server.useProxy,
                },
                useTLS: {
                    onChange: this.handleUseTLSChange(id),
                    value: server.useTLS,
                },
                conditionSet: {
                    onChange: this.handleSelectedConditionSetChange(id),
                    value: server.conditionSet,
                },
                onDelete: () => this.handleServerDelete(id),
                conditionSets: this.state.conditionSets,
                requestPath: {
                    value: server.returnPath,
                    onChange: this.handleRequestPathChange(id),
                },
            };
        });

        const props = {
            proxyUrl: {
                onChange: this.handleProxyUrlChange,
                value: this.state.proxyUrl,
            },
            tlsCertificate: {
                onChange: this.handleTlsChange("tlsCertificate"),
                value: this.state.tlsCertificate,
            },
            tlsKey: {
                onChange: this.handleTlsChange("tlsKey"),
                value: this.state.tlsKey,
            },
            servers: servers,
            onValidated: this.onValidatedChanged,
            conditionSets: this.state.conditionSets,
            onConditionsChanged: this.handleConditionsChanged,
            onAddNewServer: () => this.addServer(),
            serverIndex: this.state.serverIndex,
            serverIndexChanged: (i: number) => this.serverIndexChanged(i),
            onAddNewCondition: this.addNewCondition,
            serverStatuses: this.state.serverStatuses,
        };
        return (
            <>
                <AppInfoDialog
                    onClose={() => this.setState({ ...this.state, openInfo: false })}
                    shouldOpen={this.state.openInfo}
                />
                <Stack spacing={2}>
                    <TopBar onSave={this.onSaveButton} onInfo={this.onInfoButton} />
                    <MainPanel {...props} />
                </Stack>
            </>
        );
    }
}

export default Settings;
