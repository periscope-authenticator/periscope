import * as React from "react";
import {
    Autocomplete,
    Box,
    Button,
    FormControl,
    Grid,
    IconButton,
    InputLabel,
    ListItemIcon,
    Menu,
    MenuItem,
    Select,
    SelectChangeEvent,
    Stack,
    Tab,
    TableCell,
    Tabs,
    TextField,
    Typography,
} from "@mui/material";
import { ConditionSet, ImportedConditionSet } from "../types/server";
import {
    ConditionSources,
    ConditionStore,
    NumberComparatorType,
    NumberComparators,
    StringComparatorType,
    StringComparators,
} from "../http-condition/condition-store";
import { FileDownload, FileUpload, MoreVert } from "@mui/icons-material";
import { ImportDialog, ImportedConditionSetsWithConflicts } from "./condition-settings/import-dialog";
import { None, Option, Some } from "ts-results";
import { dropNull, isNumeric, replaceWithObject } from "../utils";
import { ChippedLabel } from "./badged-label";
import { ConditionTable } from "./condition-settings/condition-table";
import { ExportDialog } from "./condition-settings/export-dialog";
import { NotificationContext } from "./notification-context";
import RemoveCircleIcon from "@mui/icons-material/RemoveCircle";
import { TabPanel } from "./tab-panel";
import { v4 as uuid } from "uuid";

const SUPPORTED_HEADERS = [
    "Connection",
    "Content-Disposition",
    "Content-Encoding",
    "Content-Type",
    "Keep-Alive",
    "Location",
    "Refresh",
    "Server",
    "Set-Cookie",
    "WWW-Authenticate",
] as const;

type IInitialSettings = {
    [Property in ConditionSources]: Partial<ConditionStore>;
};

interface ConditionTabProps {
    conditionSets: ConditionSet[];
    onChange: (set: ConditionSet[]) => void;
    onAddNew: () => void;
}

type TabState = {
    selectedIndex: number;
    exportDialogOpen: boolean;
    importDialogOpen: boolean;
    importedConditionSets: ImportedConditionSetsWithConflicts[];
};

export const ConditionTab = (props: ConditionTabProps) => {
    const [state, setState] = React.useState<TabState>({
        selectedIndex: 0,
        exportDialogOpen: false,
        importDialogOpen: false,
        importedConditionSets: [],
    });

    const { selectedIndex, exportDialogOpen, importDialogOpen, importedConditionSets } = state;

    const { conditionSets, onChange, onAddNew } = props;

    const notifyContext = React.useContext(NotificationContext);

    const conditionTabs = conditionSets.map((set) => {
        const label = (
            <ChippedLabel color="info" chipContent="Default" hasChip={set.isDefault} size="small">
                {set.name}
            </ChippedLabel>
        );

        return <Tab key={set.id} label={label} />;
    });

    const onConditionSetChanged = (set: ConditionSet) => {
        onChange(conditionSets.map(replaceWithObject(set)));
    };

    const onDeleteClicked = (id: string) => () => {
        if (conditionSets.length === 1) {
            notifyContext.notify("Must have at least one Condition Set", "error");
            return;
        }
        onChange(conditionSets.filter((set) => set.id !== id));
        setState({ ...state, selectedIndex: 0 });
    };

    const conditionItems = conditionSets.map((conditionItem, index) => {
        return (
            <TabPanel
                key={conditionItem.id}
                selectedIndex={selectedIndex}
                index={index}
                sx={{ flexGrow: 1, display: "flex" }}
            >
                <ConditionSection
                    conditionSet={conditionItem}
                    onChange={onConditionSetChanged}
                    onDelete={onDeleteClicked(conditionItem.id)}
                />
            </TabPanel>
        );
    });

    const handleTabChange = React.useCallback(
        (_: React.SyntheticEvent<Element>, newIndex: number) => {
            setState({ ...state, selectedIndex: newIndex });
        },
        [setState, state],
    );

    const defaultSet = React.useMemo(() => {
        return conditionSets.find((set) => set.isDefault)!.id;
    }, [conditionSets]);

    const onDefaultChanged = (event: SelectChangeEvent<string>) => {
        const newDefaultID = event.target.value;
        const changedSets = conditionSets.map((set) => {
            return { ...set, isDefault: newDefaultID === set.id };
        });

        onChange(changedSets);
    };

    const defaultSelectItems = conditionSets.map((set) => {
        return (
            <MenuItem value={set.id} key={set.id}>
                {set.name}
            </MenuItem>
        );
    });

    const onNewClicked = () => {
        onAddNew();
        setState({ ...state, selectedIndex: conditionSets.length });
    };

    const onExportDialogClosed = (result: Option<string[]>) => {
        setState({ ...state, exportDialogOpen: false });
        if (result.none) {
            return;
        }

        api.exportConditionSets(result.val)
            .then((res) => {
                if (res.err) {
                    notifyContext.notify(res.val, "error");
                    return;
                }

                if (res.val.cancelled) return;

                notifyContext.notify("Saved successfully", "success");
            })
            .catch(() => {});
    };

    const onImportDialogClicked = () => {
        api.importConditionSet()
            .then((result) => {
                switch (result._type) {
                    case "error":
                        notifyContext.notify("Failed to import file: " + result.value, "error");
                        break;
                    case "file-data": {
                        const sets = result.value.map((set) => {
                            return {
                                ...set,
                                hasConflict: conditionSets.some((other) => other.name === set.name),
                            };
                        });
                        setState({ ...state, importDialogOpen: true, importedConditionSets: sets });
                        break;
                    }
                    case "cancel":
                        // Do nothing
                        break;
                }
            })
            // Should never happen but just in case.
            .catch(() => notifyContext.notify("Failed to import file", "error"));
    };

    const onImportDialogClosed = (conditions: ImportedConditionSet[]) => {
        setState({ ...state, importDialogOpen: false });
        if (conditions.length === 0) {
            return;
        }

        const newSets = conditionSets.map((set) => {
            const newSet = conditions.find((s) => s.name === set.name);
            if (newSet != null) {
                return { ...newSet, id: set.id, isDefault: set.isDefault };
            }

            return set;
        });

        conditions.forEach((newSet) => {
            // We've already added them by overriding
            if (newSets.some((set) => set.name === newSet.name)) {
                return;
            }

            newSets.push({ ...newSet, isDefault: false });
        });

        onChange(newSets);
        notifyContext.notify("Successfully imported! Press save to make changes permanent", "success");
    };

    return (
        <Stack spacing={2}>
            <ExportDialog
                conditionSets={conditionSets}
                shouldOpen={exportDialogOpen}
                onExportClosed={onExportDialogClosed}
            ></ExportDialog>
            <ImportDialog
                conditionSets={importedConditionSets}
                shouldOpen={importDialogOpen}
                onImportClosed={onImportDialogClosed}
                willOverwrite={(name) => conditionSets.some((set) => set.name === name)}
            ></ImportDialog>
            <Stack direction="row" spacing={2}>
                <FormControl fullWidth>
                    <InputLabel id="default-condition-set-label">Default</InputLabel>
                    <Select
                        labelId="default-condition-set-label"
                        id="default-condition-set"
                        value={defaultSet}
                        label="Default Conditions"
                        onChange={onDefaultChanged}
                    >
                        {defaultSelectItems}
                    </Select>
                </FormControl>
                <Button variant="text" sx={{ float: "right", flexShrink: 0 }} onClick={onNewClicked}>
                    Add New Condition Set
                </Button>
                <ImportExportMenu
                    onImportClicked={onImportDialogClicked}
                    onExportClicked={() => setState({ ...state, exportDialogOpen: true })}
                />
            </Stack>
            <Box sx={{ flexGrow: 1, bgcolor: "background.paper", display: "flex" }}>
                <Tabs
                    value={selectedIndex}
                    onChange={handleTabChange}
                    orientation="vertical"
                    sx={{ borderRight: 1, borderColor: "divider", flexShrink: 0 }}
                >
                    {conditionTabs}
                </Tabs>
                <Stack spacing="2" sx={{ flexGrow: 1, display: "flex" }}>
                    {conditionItems}
                </Stack>
            </Box>
        </Stack>
    );
};
type ImportExportMenuProps = {
    onImportClicked: () => void;
    onExportClicked: () => void;
};
const ImportExportMenu = ({ onImportClicked, onExportClicked }: ImportExportMenuProps) => {
    const [anchor, setAnchor] = React.useState<Option<HTMLElement>>(None);
    const shouldOpen = anchor.some;

    const closeDialog = () => {
        setAnchor(None);
    };

    return (
        <div>
            <IconButton onClick={(event: React.MouseEvent<HTMLButtonElement>) => setAnchor(Some(event.currentTarget))}>
                <MoreVert />
            </IconButton>
            <Menu open={shouldOpen} anchorEl={anchor.unwrapOr(undefined)} onClose={closeDialog}>
                <MenuItem
                    onClick={() => {
                        closeDialog();
                        onExportClicked();
                    }}
                >
                    <ListItemIcon>
                        <FileUpload fontSize="small"></FileUpload>
                    </ListItemIcon>
                    Export
                </MenuItem>
                <MenuItem
                    onClick={() => {
                        closeDialog();
                        onImportClicked();
                    }}
                >
                    <ListItemIcon color="primary">
                        <FileDownload fontSize="small"></FileDownload>
                    </ListItemIcon>
                    Import
                </MenuItem>
            </Menu>
        </div>
    );
};

const BlankOptionalSettings: Partial<ConditionStore> = {
    headerName: undefined,
    headerComparator: undefined,
    headerValue: undefined,
    numberComparator: undefined,
    numberValue: undefined,
} as const;

const InitialSettings: IInitialSettings = {
    header: {
        ...BlankOptionalSettings,
        headerName: SUPPORTED_HEADERS[0].toLowerCase(),
        headerComparator: StringComparators[0],
        headerValue: "",
    },
    "response-code": {
        ...BlankOptionalSettings,
        numberComparator: NumberComparators[0],
        numberValue: 0,
    },
} as const;

interface ConditionSectionProps {
    conditionSet: ConditionSet;
    onChange: (conditions: ConditionSet) => void;
    onDelete: () => void;
}

export const ConditionSection = (props: ConditionSectionProps) => {
    const { conditionSet, onChange, onDelete } = props;
    const { conditions, name } = conditionSet;

    const onNewClicked = () => {
        onChange({
            ...conditionSet,
            conditions: [...conditions, { conditionSource: "header", id: uuid(), ...InitialSettings.header }],
        });
    };

    const onNameChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = event.target.value;
        onChange({ ...conditionSet, name: newValue });
    };

    return (
        <Stack spacing={3}>
            <Stack direction="row" spacing={2}>
                <TextField id="setName" label="Name" value={name} onChange={onNameChanged} fullWidth />
                <Button color="error" variant="contained" onClick={onDelete}>
                    Delete
                </Button>
            </Stack>
            <Grid container spacing={2} columns={13}>
                <Grid item xs={10}>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1, float: "left" }}>
                        Conditions
                    </Typography>
                </Grid>
                <Grid item xs={3}>
                    <Button onClick={onNewClicked}>Add New Condition</Button>
                </Grid>
                {conditions.length === 0 ? (
                    "No conditions set"
                ) : (
                    <ConditionTable conditionSet={conditionSet} onChange={onChange} />
                )}
            </Grid>
        </Stack>
    );
};

interface RowProps {
    condition: ConditionStore;
    onChange: (condition: ConditionStore) => void;
    onRemove: (id: string) => void;
}

export const ConditionRow = (props: RowProps) => {
    const { condition, onChange, onRemove } = props;

    const sources = [
        ["header", "Header"],
        ["response-code", "Response Status Code"],
    ];

    const onSelectChange = (conditionSource: ConditionSources) => {
        onChange({
            ...condition,
            conditionSource,
            ...InitialSettings[conditionSource],
        });
    };

    return (
        <>
            <TableCell>
                <ConditionTypeSelect
                    sources={sources}
                    type={condition.conditionSource}
                    onChange={onSelectChange}
                ></ConditionTypeSelect>
            </TableCell>
            <ConditionSwitch condition={condition} onChange={onChange}></ConditionSwitch>
            <TableCell>
                <ConditionRemove onClick={() => onRemove(condition.id)}></ConditionRemove>
            </TableCell>
        </>
    );
};

interface TypeSelectProps {
    sources: string[][];
    type: string;
    onChange: (conditionSource: ConditionSources) => void;
}

const ConditionTypeSelect = (props: TypeSelectProps) => {
    const { sources, type, onChange } = props;

    const onSelectChange = (selectEvent: SelectChangeEvent<string>) => {
        onChange(selectEvent.target.value as ConditionSources);
    };

    return (
        <FormControl fullWidth>
            <InputLabel>Source</InputLabel>
            <Select label="Source" value={type} onChange={onSelectChange}>
                {sources.map(([id, text]) => {
                    return (
                        <MenuItem key={id} value={id}>
                            {text}
                        </MenuItem>
                    );
                })}
            </Select>
        </FormControl>
    );
};

interface ConditionSwitchProps {
    condition: ConditionStore;
    onChange: (filter: ConditionStore) => void;
}

const ConditionSwitch = (props: ConditionSwitchProps) => {
    const { condition, onChange } = props;

    const dict = {
        "response-code": () => (
            <ConditionResponseOptions condition={condition} onChange={onChange}></ConditionResponseOptions>
        ),
        header: () => <ConditionFilterOptions condition={condition} onChange={onChange}></ConditionFilterOptions>,
    };
    return dict[condition.conditionSource]();
};

interface ConditionProps {
    condition: ConditionStore;
    onChange: (condition: ConditionStore) => void;
}

const ConditionFilterOptions = (props: ConditionProps) => {
    const { headerName, headerComparator, headerValue } = props.condition;
    const { onChange } = props;

    const headerNameChanged = (_event: React.SyntheticEvent, value: string | null) => {
        onChange({ ...props.condition, headerName: value });
    };

    const headerOperatorChanged = (event: SelectChangeEvent<string>) => {
        onChange({
            ...props.condition,
            headerComparator: event.target.value as StringComparatorType,
        });
    };

    const headerValueChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
        onChange({ ...props.condition, headerValue: event.target.value });
    };

    return (
        <>
            <TableCell>
                <FormControl fullWidth>
                    <Autocomplete
                        freeSolo
                        options={SUPPORTED_HEADERS}
                        renderInput={(params) => <TextField {...params} label="Header" />}
                        value={dropNull(headerName)}
                        onChange={headerNameChanged}
                    />
                </FormControl>
            </TableCell>
            <TableCell>
                <FormControl fullWidth>
                    <InputLabel>Operator</InputLabel>
                    <Select label="Operator" value={dropNull(headerComparator)} onChange={headerOperatorChanged}>
                        {StringComparators.map((operator) => {
                            return (
                                <MenuItem key={operator} value={operator}>
                                    {operator}
                                </MenuItem>
                            );
                        })}
                    </Select>
                </FormControl>
            </TableCell>
            <TableCell>
                <TextField fullWidth label="Value" value={headerValue} onChange={headerValueChanged}></TextField>
            </TableCell>
        </>
    );
};

const ConditionResponseOptions = (props: ConditionProps) => {
    const { condition: filter, onChange } = props;

    const operatorChanged = (event: SelectChangeEvent<string>) => {
        onChange({
            ...filter,
            numberComparator: event.target.value as NumberComparatorType,
        });
    };

    const valueChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
        const stringValue = event.target.value;
        if (isNumeric(stringValue)) {
            onChange({ ...filter, numberValue: Number(stringValue) });
            return;
        }

        onChange({ ...filter, numberValue: 0 });
    };

    return (
        <>
            <TableCell></TableCell>
            <TableCell>
                <FormControl fullWidth>
                    <InputLabel>Operator</InputLabel>
                    <Select label="Operator" value={dropNull(filter.numberComparator)} onChange={operatorChanged}>
                        {NumberComparators.map((operator) => {
                            return (
                                <MenuItem key={operator} value={operator}>
                                    {operator}
                                </MenuItem>
                            );
                        })}
                    </Select>
                </FormControl>
            </TableCell>
            <TableCell>
                <TextField
                    inputProps={{ inputMode: "numeric", pattern: "[0-9]" }}
                    value={filter.numberValue}
                    onChange={valueChanged}
                    fullWidth
                ></TextField>
            </TableCell>
        </>
    );
};

interface ConditionRemoveProps {
    onClick?: () => void;
}

const ConditionRemove = (props: ConditionRemoveProps) => {
    return (
        <IconButton onClick={props.onClick} style={{ float: "right" }}>
            <RemoveCircleIcon></RemoveCircleIcon>
        </IconButton>
    );
};
