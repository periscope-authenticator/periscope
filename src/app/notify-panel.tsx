import * as React from 'react';
import { Alert, Collapse, Stack } from '@mui/material';
import { NotificationContext, NotificationType } from './notification-context';
import { TransitionGroup } from 'react-transition-group';

import { v4 as uuidv4 } from 'uuid';

interface Notification {
    message: string,
    type: NotificationType,
    key: string,
}
/**
 * Subscribes to the NotificationContext and displays all notifications pushed onto the Queue
 */
export const NotifyPanel = () => {

    const [notifications, setNotifications] = React.useState<Notification[]>([]);

    const onClose = (index: number) => () => {
        const cur = [...notifications];
        cur.splice(index, 1);
        setNotifications(cur);
    };

    const notificationContext = React.useContext(NotificationContext);

    React.useEffect(() => {
        const callback = (message: string, type: NotificationType) => {
            setNotifications(current => [...current, { message, type, key: uuidv4() }]);
        };

        notificationContext.onNotification(callback);

        // Removes the listener when the component is unmounted so we don't have a new subscription every time
        return () => notificationContext.removeListener(callback);
    }, [notificationContext]);

    const renderNotification = (notification: Notification, index: number) => {
        return <Alert onClose={onClose(index)} severity={notification.type}>{notification.message}</Alert>;
    };

    return (
        <Stack spacing={2}>
            <TransitionGroup component={null}/* null makes it so it doesn't create a wrapper element to mess up the Stack */>
                {
                    notifications.map((notification, index) => (
                        <Collapse key={notification.key}>
                            {renderNotification(notification, index)}
                        </Collapse>
                    ))
                }
            </TransitionGroup>
        </Stack>
    );
};
