import * as React from 'react';

import { TextField, TextFieldProps } from '@mui/material';

export interface ValidateReturn {
    validationText?: string;
    isValid: boolean;
}

interface ValidateExtraProps {
    /**
     * Is called onBlur in the TextField so that the validation setting can be updated
     */
    validateCallback: (event: HTMLInputElement) => ValidateReturn;
}

type ValidatedFieldProps = ValidateExtraProps & TextFieldProps;

/**
 * Field which performs validation on blur and shows error if validation fails
 * @param props props for the field
 */
export const ValidatedField = (props: ValidatedFieldProps): JSX.Element => {

    const [isValid, setIsValid] = React.useState(true);
    const [_invalidMessage, setInvalidMessage] = React.useState<string | undefined>("");

    const { validateCallback, ...other } = props;

    const onBlur = (event: React.FocusEvent<HTMLInputElement>) => {
        const output = validateCallback(event.target);
        const valid = output.isValid;
        setIsValid(valid);
        if (!valid) {
            setInvalidMessage(output.validationText);
        }
    };

    return (
        <TextField onBlur={onBlur} {...other} error={!isValid} />
    );
};
