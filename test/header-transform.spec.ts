import * as sinon from "sinon";

import { HeaderTransform, PERISCOPE_USER_AGENT } from "../src/header-transform";
import { suite, test } from "mocha";
import { assert } from "chai";

const serverInfo = {
    name: "Server",
    listenPort: "1234",
    proxyAddress: "example.com",
    proxyPort: "443",
};

const buildServerManager = () => {
    return {
        stopAllAsync: () => {
            return Promise.all([]) as Promise<void[]>;
        },
        stopAll: () => {
            /* Intentionally empty */
        },
        localForRemoteAddress: (hostToFind: URL) => hostToFind.toString(),
        serverStatuses: () => {
            return [];
        },
    };
};

suite("HeaderTransform", function () {
    suite("#transformHeadersForRemoteRequest", function () {
        test("should not return the same object", function () {
            const serverManager = buildServerManager();

            const headerTransform = new HeaderTransform(serverManager);
            headerTransform.newServerInfo({ ...serverInfo });
            const origHeaders = {};
            const newHeaders = headerTransform.transformHeadersForRemoteRequest(origHeaders);

            assert.notStrictEqual(newHeaders, origHeaders);
        });

        test("set user agent to periscope if null", function () {
            const headerTransform = new HeaderTransform(buildServerManager());

            headerTransform.newServerInfo({ ...serverInfo });
            const newHeaders = headerTransform.transformHeadersForRemoteRequest({});

            assert.equal(newHeaders["user-agent"], PERISCOPE_USER_AGENT);
        });

        test("append user agent to existing user-agent", function () {
            const headerTransform = new HeaderTransform(buildServerManager());
            headerTransform.newServerInfo({ ...serverInfo });

            const existingUserAgent = "user agent";
            const newHeaders = headerTransform.transformHeadersForRemoteRequest({
                "user-agent": existingUserAgent,
            });

            const newUserAgent = newHeaders["user-agent"];
            assert.include(newUserAgent, existingUserAgent);
            assert.include(newUserAgent, PERISCOPE_USER_AGENT);
        });

        test("set host to proxyAddress if null", function () {
            const headerTransform = new HeaderTransform(buildServerManager());

            headerTransform.newServerInfo({ ...serverInfo });

            const newHeaders = headerTransform.transformHeadersForRemoteRequest({});

            const newHost = newHeaders.host;
            assert.equal(newHost, serverInfo.proxyAddress);
        });

        test("set host to proxyAddress if set", function () {
            const headerTransform = new HeaderTransform(buildServerManager());
            headerTransform.newServerInfo({ ...serverInfo });
            const newHeaders = headerTransform.transformHeadersForRemoteRequest({
                host: "test.example.com",
            });

            const newHost = newHeaders.host;
            assert.equal(newHost, serverInfo.proxyAddress);
        });

        test("set cookie to empty on initialize", function () {
            const headerTransform = new HeaderTransform(buildServerManager());
            headerTransform.newServerInfo({ ...serverInfo });
            const newHeaders = headerTransform.transformHeadersForRemoteRequest({});

            const newCookies = newHeaders.cookie;

            assert.equal(newCookies, "");
        });

        test("set cookie to value after onNewCookies", function () {
            const headerTransform = new HeaderTransform(buildServerManager());
            headerTransform.newServerInfo({ ...serverInfo });
            const expectedCookie = "a wild cookie";
            headerTransform.onNewCookies(expectedCookie);
            const newHeaders = headerTransform.transformHeadersForRemoteRequest({});

            assert.equal(newHeaders.cookie, expectedCookie);
        });

        test("pass through non-changed headers", function () {
            const headerTransform = new HeaderTransform(buildServerManager());
            headerTransform.newServerInfo({ ...serverInfo });
            const expectedHeader = "this is a header";

            const newHeaders = headerTransform.transformHeadersForRemoteRequest({
                "content-length": expectedHeader,
            });

            assert.equal(newHeaders["content-length"], expectedHeader);
        });
    });

    suite("#transformHeaderForClientResponse", function () {
        test("does not return the same object", function () {
            const headerTransform = new HeaderTransform(buildServerManager());
            headerTransform.newServerInfo({ ...serverInfo });
            const originalHeaders = {};
            const newHeaders = headerTransform.transformHeaderForClientResponse({}, originalHeaders);

            assert.notStrictEqual(newHeaders, originalHeaders);
        });

        test("returns null for a null input location", function () {
            const headerTransform = new HeaderTransform(buildServerManager());
            headerTransform.newServerInfo({ ...serverInfo });
            const newHeaders = headerTransform.transformHeaderForClientResponse({}, {});

            assert.equal(newHeaders.location, null);
        });

        test("returns original for relative location", function () {
            const headerTransform = new HeaderTransform(buildServerManager());
            const location = "/location";
            const newHeaders = headerTransform.transformHeaderForClientResponse({}, { location: location });

            assert.equal(newHeaders.location, location);
        });

        test("returns result of ServerManager if full path", function () {
            const serverManager = buildServerManager();

            const headerTransform = new HeaderTransform(serverManager);
            const newHost = "https://example122.com";
            const stub = sinon.stub(serverManager, "localForRemoteAddress");
            stub.returns(newHost);

            const newHeaders = headerTransform.transformHeaderForClientResponse(
                {},
                { location: "https://example.com" }
            );

            assert.equal(newHeaders.location, newHost);
        });

        test("returns null if www-auth is null", function () {
            const headerTransform = new HeaderTransform(buildServerManager());

            const newHeaders = headerTransform.transformHeaderForClientResponse({}, {});
            assert.equal(newHeaders["www-authenticate"], null);
        });

        test("returns same if www-auth has no realm", function () {
            const headerTransform = new HeaderTransform(buildServerManager());
            const authHeader = 'Basic hello="hello"';
            const newHeaders = headerTransform.transformHeaderForClientResponse({}, { "www-authenticate": authHeader });

            assert.equal(newHeaders["www-authenticate"], authHeader);
        });

        test("replaces url in realm for www-auth", function () {
            const serverManager = buildServerManager();
            const stub = sinon.stub(serverManager, "localForRemoteAddress");
            stub.returns("http://localhost");

            const headerTransform = new HeaderTransform(serverManager);
            const newHeaders = headerTransform.transformHeaderForClientResponse(
                {},
                { "www-authenticate": 'realm="https://example.com"' }
            );

            assert.include(newHeaders["www-authenticate"], "http://localhost");
            assert.include(newHeaders["www-authenticate"], "realm=");
            assert.notInclude(newHeaders["www-authenticate"], "example.com");
        });

        test("returns the original if realm is not a valid URL", function () {
            const serverManager = buildServerManager();
            const stub = sinon.stub(serverManager, "localForRemoteAddress");
            stub.returns("http://localhost");
            stub.withArgs(undefined).throws();

            const originalWWWAuth = 'realm="hi"';

            const headerTransform = new HeaderTransform(serverManager);
            const newHeaders = headerTransform.transformHeaderForClientResponse(
                {},
                { "www-authenticate": originalWWWAuth }
            );

            assert.equal(newHeaders["www-authenticate"], originalWWWAuth);
        });
    });
});
