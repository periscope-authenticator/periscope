import * as React from "react";
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material";
import { ConditionRow } from "../condition-settings";
import { ConditionSet } from "../../types/server";
import { ConditionStore } from "../../http-condition/condition-store";

interface TableProps {
    conditionSet: ConditionSet;
    onChange: (conditionSet: ConditionSet) => void;
}
export const ConditionTable = (props: TableProps) => {
    const { conditionSet, onChange } = props;
    const { conditions } = conditionSet;

    const onConditionsChanged = (conditions: ConditionStore[]) => {
        onChange({
            ...conditionSet,
            conditions: conditions,
        });
    };

    const generateOnChange = (index: number) => (condition: ConditionStore) => {
        const newConditions = [...conditions];
        newConditions[index] = condition;
        onConditionsChanged(newConditions);
    };

    const onRemove = (id: string) => {
        const newConditions = conditions.filter((condition) => condition.id !== id);

        onConditionsChanged(newConditions);
    };

    return (
        <TableContainer>
            <Table sx={{ minWidth: 650 }}>
                <TableHead>
                    <TableRow>
                        <TableCell>Source</TableCell>
                        <TableCell></TableCell>
                        <TableCell>Operator</TableCell>
                        <TableCell></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {conditions.map((row, index) => (
                        <TableRow key={row.id}>
                            <ConditionRow
                                condition={row}
                                onChange={generateOnChange(index)}
                                onRemove={() => onRemove(row.id)}
                            />
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
};
