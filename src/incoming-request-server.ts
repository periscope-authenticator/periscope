import * as http from 'http';
import * as https from 'https';
import EventEmitter from 'events';
import { StatusType } from './types/server';
import { TLSCertificateManager } from './tls-certificate-manager';
import log from 'electron-log/main';

export interface IncomingRequestServer {
    /**
     * Start listening on the port provided in the background
     * Does not wait for the listener to start.
     */
    start(): void;
    /**
      * Start listening and await for the listener to start
      */
    startAsync(): Promise<void>;
    /**
      * Stop listening on the port in the background
      * Does not wait for the listener to stop.
      * Server can be restarted
      */
    stop(): void;
    /**
      * Stop listening on the port and await for the listener to stop
      * Server can be restarted
      */
    stopAsync(): Promise<void>;
    /**
      * Change the port the listener is listening on. Change will happen in the backgroun
      * A 'stopped' and 'running' status change will happen when the change is complete
      */
    updateServerPort(port: number): void;

    /**
      * Get ready for the server to be shut down completely
      */
    cleanupServer(): void;


    /**
      * Listen for error events
      */
    on(event: 'error', callback: (reason: string) => void): void;
    /**
      * Listen for request events
      */
    on(event: 'request', callback: (request: http.IncomingMessage, response: http.ServerResponse<http.IncomingMessage>) => void): void;
    /**
      * Listen for status change events
      */
    on(event: 'status-update', callback: (status: StatusType) => void): void;

    /**
      * Stop listening for error events
      */
    off(event: 'error', callback: (reason: string) => void): void;
    /**
      * Stop listening for request events
      */
    off(event: 'request', callback: (request: http.IncomingMessage, response: http.ServerResponse<http.IncomingMessage>) => void): void;
    /**
      * Stop listening for status update events
      */
    off(event: 'status-update', callback: (status: StatusType) => void): void;
}

export function buildIncomingRequestServer(serverPort: number, useTLS: boolean, certificateManager: TLSCertificateManager,
    errorHandler?: (reason: string) => void): IncomingRequestServer {
    if (useTLS) {
        log.debug('Building new HTTPS server');
        return new HttpsRequestServer(serverPort, certificateManager, errorHandler);
    }
    log.debug('Building new HTTP server');
    return new HttpRequestServer(serverPort, errorHandler);
}

class HttpRequestServer extends EventEmitter implements IncomingRequestServer {
    private serverPort: number;
    private server: http.Server;

    public constructor(serverPort: number, errorHandler?: (reason: string) => void) {
        super();
        if (errorHandler) {
            this.on('error', errorHandler);
        }

        this.serverPort = serverPort;
        this.server = http.createServer((req, res) => this.emit('request', req, res));

        this.server.on('listening', () => this.emit('status-update', 'running'));
        this.server.on('error', (e) => this.emit('error', e.message));
        this.server.on('close', () => this.emit('status-update', 'stopped'));
    }

    public build() {
    }

    start() {
        this.server.listen(this.serverPort);
    }

    startAsync(): Promise<void> {
        return new Promise((resolve, reject) => {
            if (this.server.listening) {
                resolve();
                return;
            }

            this.server.once('listening', () => resolve());
            this.server.once('error', () => reject());
            this.start();
        });
    }

    stop() {
        this.server.close();
    }
    cleanupServer(): void {/* nothing to clean */}

    stopAsync(): Promise<void> {
        return new Promise((resolve, reject) => {
            if (!this.server.listening) {
                resolve();
                return;
            }

            this.server.once('close', () => resolve());
            this.server.once('error', () => reject());
            this.stop();
        });
    }

    updateServerPort(port: number): void {
        this.serverPort = port;
        this.server.once('close', () => this.start());
        this.stop();
    }
}

interface HttpsError {
    code?: string;
    reason: string;
}

// We need any because it could be any type
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function isHttpsError(object: any): object is HttpsError {
    return 'reason' in object;
}

class HttpsRequestServer extends EventEmitter implements IncomingRequestServer {
    private serverPort: number;
    private server?: https.Server;
    private certificateManager: TLSCertificateManager;

    public constructor(serverPort: number, certificateManager: TLSCertificateManager, errorHandler?: (reason: string) => void) {
        super();
        if (errorHandler) {
            this.on('error', errorHandler);
        }

        this.serverPort = serverPort;
        this.certificateManager = certificateManager;

        this.createServer();
        certificateManager.on('new-cert-loaded', this.newCertLoaded);
    }

    cleanupServer(): void {
        // Need to deregister this event as it is no longer necessary and prevents garbage collection
        this.certificateManager.off('new-cert-loaded', this.newCertLoaded);
    }

    private newCertLoaded = () => {
        this.stopAsync().catch((e) => {
            log.error('Failed to stop previous server when creating new server', e);
        }).finally(() => {
            log.info('Creating new server from new certificates');
            this.createServer();
            this.start();
        });
    };

    private createServer() {
        const option = this.certificateManager.certAndKey();
        if (option.none) {
            this.emit('error', `No certificate and key found: ${this.certificateManager.error}`);
            return;
        }
        const { cert, key } = option.val;
        try {
            this.server = https.createServer({ cert, key }, (req, res) => this.emit('request', req, res));
            log.info('Created https Server on port', this.serverPort);

            this.server.on('listening', () => this.emit('status-update', 'running'));
            this.server.on('error', (e) => this.emit('error', e.message));
            this.server.on('close', () => this.emit('status-update', 'stopped'));
        } catch (e) {
            this.server = undefined;
            if (isHttpsError(e)) {
                this.emit('error', opensslErrorNiceName(e.reason));
            } else if (e instanceof Error) {
                this.emit('error', `${e.name} ${e.message}`);
            }

            log.error("Failed to create httpsServer", e);
        }
    }

    start() {
        this.server?.listen(this.serverPort);
    }

    startAsync(): Promise<void> {
        return new Promise((resolve, reject) => {
            if (!this.server) {
                reject();
                return;
            }

            if (this.server.listening) {
                resolve();
                return;
            }

            this.server.once('listening', () => resolve());
            this.server.once('error', () => reject());
            this.start();
        });
    }

    stop() {
        this.server?.close();
    }

    stopAsync(): Promise<void> {
        return new Promise((resolve, reject) => {
            if (!this.server) {
                resolve();
                return;
            }

            if (!this.server.listening) {
                resolve();
                return;
            }

            this.server.once('close', () => resolve());
            this.server.once('error', () => reject());
            this.stop();
        });
    }

    updateServerPort(port: number): void {
        this.serverPort = port;
        if (this.server?.listening) {
            this.server?.once('close', () => this.start());
            this.stop();
            return;
        }

        this.start();
    }
}

function opensslErrorNiceName(errorCode: string): string {
    switch (errorCode) {
        case 'BAD_PASSWORD_READ': return 'TLS Keys with passphrases are not supported';
        case 'KEY_VALUES_MISMATCH': return 'Certificate and key do not match';
        case 'CERT_HAS_EXPIRED': return 'Certificate has expired.';
        case 'CERT_NOT_YET_VALID': return 'Certificate is not yet valid.';
        case 'UNABLE_TO_DECRYPT_CERT_SIGNATURE': return 'Unable to decrypt certificate\'s signature.';
        case 'CERT_SIGNATURE_FAILURE': return 'Certificate signature failure.';
        case 'CERT_REVOKED': return 'Certificate revoked.';
        case 'NO_START_LINE': return 'Invalid certificate or key';
        default:
            return errorCode;
    }
}
