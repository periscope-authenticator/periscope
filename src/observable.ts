type ResolveFunc<Output> = (value: Output) => void;

type SubscriberFunctions<Output> = [ResolveFunc<Output>, (error: Error) => void];

/**
 * Naive implementation of rxjs's Observable with more use of Promises
 */
export class Observable<Output> {
    private subscribers: SubscriberFunctions<Output>[] = [];

    /**
     * Returns a promise which resolves or rejects when the observable has a value or an error
     * Resolve and rejections must not throw errors or they will disrupt the entire list of subscribers
     * @returns {Promise<Output>} Promise the resolves/rejects when the observable has a value or an error
     */
    public subscribe(): Promise<Output> {
        return new Promise((resolve, reject) => {
            this.subscribers.push([resolve, reject]);
        });
    }

    /**
     * Pushes a value to the subscribers resolving their promises
     * @param value Value to push to subscribers
     */
    public pushValue(value: Output) {
        this.subscribers.forEach(([resolve, _]) => {
            resolve(value);
        });
    }

    /**
     * Pushes an error to the subscribers which rejects their promises
     * @param error Error for the subscribers
     */
    public pushError(error: Error) {
        this.subscribers.forEach(([_, reject]) => {
            reject(error);
        });
    }
}
