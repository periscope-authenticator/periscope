import * as React from "react";
import { Dialog, DialogContent, DialogTitle, List, ListItemText } from "@mui/material";
import { None, Option, Some } from "ts-results";
import log from "electron-log/renderer";
import { version } from "../../package.json";

export const AppInfoDialog = ({ shouldOpen, onClose }: { shouldOpen: boolean; onClose: () => void }) => {
    const logFileClicked = () => {
        api.openLogFileInExplorer().catch((err) => log.error("Failed to open log", err));
    };
    const [logFile, setLogFile] = React.useState<Option<string>>(None);
    React.useEffect(() => {
        api.logFilePath()
            .then((path) => {
                setLogFile(Some(path));
            })
            .catch((err) => log.error("Failed to get log path", err));
    });
    return (
        <Dialog open={shouldOpen} onClose={onClose}>
            <DialogTitle>Periscope Authenticator</DialogTitle>
            <DialogContent>
                <List>
                    <ListItemText primary={`Version: ${version}`} />
                    <ListItemText
                        primary={
                            <span>
                                Log File:{" "}
                                <a href="javascript:void(0)" onClick={logFileClicked}>
                                    {logFile.some ? logFile.val : "..."}
                                </a>
                            </span>
                        }
                    />
                </List>
            </DialogContent>
        </Dialog>
    );
};
