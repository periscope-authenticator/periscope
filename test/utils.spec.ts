import { realmFromWWWAuthenticateHeader, trimLeadingSlash, trimTrailingSlash, tryIntoUrl } from "../src/utils";
import { suite, test } from "mocha";
import { assert } from "chai";

suite("utils", function() {
    suite("realmFromWWWAuthenticateHeader", function() {
        test("should return null for null", function() {
            const result = realmFromWWWAuthenticateHeader(null);
            assert.equal(result, null);
        });

        test("should return null for empty", function() {
            const result = realmFromWWWAuthenticateHeader("");
            assert.equal(result, null);
        });

        test("should return null for no realm", function() {
            const result = realmFromWWWAuthenticateHeader("Basic ");
            assert.equal(result, null);
        });

        test("should return realm with realm", function() {
            const result = realmFromWWWAuthenticateHeader('Basic realm="this-is-a-realm"');
            assert.equal(result?.realm, "this-is-a-realm");
        });

        test("should return realmStart at the first character", function() {
            const result = realmFromWWWAuthenticateHeader('realm="a-realm"');
            assert.equal(result?.realmStart, 7);
        });

        test("should return realmStart at the next character after realm", function() {
            const result = realmFromWWWAuthenticateHeader('realm="a-realm"');
            assert.equal(result?.realmEnd, 14);
        });
    });

    suite("tryIntoUrl", function() {
        test("should return null for null", function() {
            // @ts-expect-error Making sure this doesn't break in JS
            const result = tryIntoUrl(null);
            assert.equal(result, null);
        });

        test("should return null for empty string", function() {
            const result = tryIntoUrl("");
            assert.equal(result, null);
        });

        test("should return null for a non-empty non-url string", function() {
            const result = tryIntoUrl("non-empty");
            assert.equal(result, null);
        });

        test("should return a URL for a valid http url", function() {
            const result = tryIntoUrl("http://localhost");
            assert.instanceOf(result, URL);
        });

        test("should return a URL for a valid https url", function() {
            const result = tryIntoUrl("https://google.com");
            assert.instanceOf(result, URL);
        });

        test("should return a URL for a fully-loaded https url", function() {
            const result = tryIntoUrl("https://example.com:19999/path/to/something?hi=hello");
            assert.instanceOf(result, URL);
        });
    });

    suite("trimLeadingSlash", function() {
        const testFunc = (input: string, expected: string) =>
            function() {
                const result = trimLeadingSlash(input);
                assert.equal(result, expected);
            };

        test("should return same for no slash", testFunc("noslash", "noslash"));
        test("should trim slash but keep rest", testFunc("/test", "test"));
        test("should only trim first slash", testFunc("//hello", "/hello"));
    });

    suite("trimtrailingSlash", function() {
        const testFunc = (input: string, expected: string) =>
            function() {
                const result = trimTrailingSlash(input);
                assert.equal(result, expected);
            };

        test("should return same for no slash", testFunc("noslash", "noslash"));
        test("should trim slash but keep rest", testFunc("test/", "test"));
        test("should only trim first slash", testFunc("hello//", "hello/"));
        test("leave leading slash alone", testFunc("/hello/", "/hello"));
    });
});
