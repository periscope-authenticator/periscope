import type { Configuration } from 'webpack';

import { rules } from './webpack.rules';
import { plugins } from './webpack.plugins';

rules.push({
    test: /\.(css|scss)$/,
    use: [
        { loader: 'style-loader' },
        { loader: 'css-loader' },
        {
            loader: 'postcss-loader',
            options: {
                postcssOptions: {
                    plugins: function () {
                        return [
                            require('autoprefixer')
                        ];
                    }
                }
            }
        }
    ]
},
    {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
    },
    {
        test: /\.(woff|woff2|ttf|otf|eot)$/,
        type: 'asset/resource',
    }
);

export const rendererConfig: Configuration = {
    module: {
        rules,
    },
    devtool: 'inline-source-map',
    plugins,
    resolve: {
        extensions: ['.js', '.ts', '.jsx', '.tsx', '.css'],
    }
};
