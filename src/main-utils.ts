// This file exists because the renderer side cannot have access to node/electron imports
import { NativeImage, nativeImage } from 'electron';
import iconPath from './resources/periscopeIcon.png';
import iconPathSmall from './resources/periscope-npm.png';
import path from 'path';

export function periscopeIconPath(): string {
    return path.resolve(__dirname, iconPath);
}

export function periscopeIcon(): NativeImage {
    const resolvedPath = periscopeIconPath();
    return nativeImage.createFromPath(resolvedPath);
}

export function periscopeIconSmallPath(): string {
    return path.resolve(__dirname, iconPathSmall);
}


export * from './utils';
