import api from './types/ipc-api';
import { contextBridge } from 'electron';

contextBridge.exposeInMainWorld('api', api);
