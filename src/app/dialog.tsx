import * as React from "react";
import { Box, SxProps } from "@mui/material";

export const Dialog = ({ children, requestId, sx }: React.PropsWithChildren<{ requestId: string; sx?: SxProps }>) => {
    const boundingBox = React.useRef<HTMLElement | null>(null);
    const sxWithDefaults: SxProps = {
        minWidth: 400,
        minHeight: 300,
        width: "fit-content",
        height: "fit-content",
        padding: "1em",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        ...sx
    };

    React.useEffect(() => {
        if (boundingBox.current == null) return;

        const rect = boundingBox.current.getBoundingClientRect();
        api.registerDialogWindowSize(requestId, { width: rect.width, height: rect.height }).catch(() => {});
    }, [boundingBox, requestId]);

    return (
        <Box sx={sxWithDefaults} ref={boundingBox}>
            {children}
        </Box>
    );
};
