import type { ForgeConfig } from '@electron-forge/shared-types';
import MakerSquirrel from '@electron-forge/maker-squirrel';
import MakerZIP from '@electron-forge/maker-zip';
import MakerDeb from '@electron-forge/maker-deb';
import MakerRpm from '@electron-forge/maker-rpm';
import WebpackPlugin from '@electron-forge/plugin-webpack';
import path from 'path';

import { mainConfig } from './webpack.main.config';
import { rendererConfig } from './webpack.renderer.config';

const config: ForgeConfig = {
    packagerConfig: {
        icon: path.resolve(__dirname, "./src/resources/periscopeIcon"),
        name: "Periscope",
        executableName: "periscope",
    },
    rebuildConfig: {},
    publishers: [],
    makers: [
        new MakerSquirrel({
            setupIcon: path.resolve(__dirname, "./src/resources/periscopeIcon.ico"),
            name: "periscope",
        }),
        new MakerZIP({}, ["darwin"]),
        new MakerRpm({
            options: {
                icon: path.resolve(__dirname, "./src/resources/periscopeIcon.png"),
            },
        }),
        new MakerDeb({
            options: {
                icon: path.resolve(__dirname, "./src/resources/periscopeIcon.png"),
            },
        }),
    ],
    plugins: [
        new WebpackPlugin({
            mainConfig,
            renderer: {
                config: rendererConfig,
                entryPoints: [
                    {
                        html: "./src/index.html",
                        js: "./src/render-app.tsx",
                        name: "main_window",
                        preload: {
                            js: "./src/preload.ts",
                        },
                    },
                ],
            },
            port: 3001,
            loggerPort: 9001,
        }),
    ],
};

export default config;
