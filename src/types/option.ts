type Option<Key extends string, Value = undefined> = {
    _type: Key;
    value: Value;
};

export const makeOption =
    <U extends string, T = undefined>(type: U) =>
    (value: T): Option<U, T> => ({ _type: type, value: value });
