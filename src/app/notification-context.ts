import * as React from 'react';
import EventEmitter from 'events';

export type NotificationType = 'info' | 'warning' | 'error' | 'success'

interface NotificationEvents {
    'new-notification': (message: string, type: NotificationType) => void;
}
class NotificationEmitter extends EventEmitter {
    public on<U extends keyof NotificationEvents>(event: U, listener: NotificationEvents[U]): this {
        return super.on(event, listener);
    }

    public emit<U extends keyof NotificationEvents>(event: U, ...args: Parameters<NotificationEvents[U]>): boolean {
        return super.emit(event, ...args);
    }

    public off<U extends keyof NotificationEvents>(event: U, listener: NotificationEvents[U]): this {
        return super.off(event, listener);
    }
}

/**
 * Handles the queue for notifications
 */
export class NotificationEventQueue {

    private emitter: NotificationEmitter;

    constructor() {
        this.emitter = new NotificationEmitter;
    }

    /**
     * Add a new notification to the queue
     * @param message Notification message
     * @param type Notification type
     */
    public notify(message: string, type: NotificationType = 'info'): void {
        this.emitter.emit('new-notification', message, type);
    }

    /**
     * Subscribes to the new notification event
     * @param eventHandler Callback to call when a new notification is pushed
     */
    public onNotification(eventHandler: (message: string, type: NotificationType) => void): void {
        this.emitter.on('new-notification', eventHandler);
    }

    /**
     * Removes a subscription from the event queue
     * @param eventHandler Handler to unsubscribe
     */
    public removeListener(eventHandler: (message: string, type: NotificationType) => void): void {
        this.emitter.off('new-notification', eventHandler);
    }
}

export const NotificationContext = React.createContext(new NotificationEventQueue());
