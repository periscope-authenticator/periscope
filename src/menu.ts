import { Menu, app } from "electron";
import { IFrontendAPI } from "./frontend-api";

const isMac = process.platform === "darwin";

export const buildMenu = (frontendApi: IFrontendAPI): Menu => {
    const menu: Electron.MenuItemConstructorOptions[] = [
        // On macOS the first item is always the app name so we add one so we can have the File menu
        // From Electron docs: https://www.electronjs.org/docs/latest/api/menu#main-menus-name
        // Taken from Electron example for Menu: https://www.electronjs.org/docs/latest/api/menu#examples
        ...(isMac ? [{ label: app.name }] : []),
        {
            label: "File",
            submenu: [{ role: "close" }, { label: "Quit", click: () => app.quit() }],
        },
        {
            label: "View",
            submenu: [
                { role: "reload" },
                { role: 'toggleDevTools' },
                { type: "separator" },
                { role: "resetZoom" },
                { role: "zoomIn" },
                { role: "zoomOut" },
                { type: "separator" },
                { role: "togglefullscreen" },
            ],
        },
        {
            label: "Help",
            submenu: [{ label: "About", click: () => frontendApi.openAboutMenu() }],
        },
    ];

    return Menu.buildFromTemplate(menu);
};
