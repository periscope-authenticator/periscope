module.exports = {
    extends: [".eslintrc.json"], // the main eslintrc file
    parserOptions: {
        tsconfigRootDir: __dirname,
    },
};
