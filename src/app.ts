import {
    BrowserWindow,
    IpcMainInvokeEvent,
    Menu,
    OpenDialogOptions,
    Tray,
    app,
    dialog,
    ipcMain,
    shell,
} from "electron";
import { Cancelled, ConditionImportResult, FileData, FileError } from "./types/import-file-result";
import { ConditionSetSchema, ProxySettings, generateIdsForConditionSet } from "./types/server";
import { DialogError, FileDialogOptions, FileDialogResult, FilePath, MultiFilePath } from "./types/file-dialog";
import { Err, Ok, Result } from "ts-results";
import installExtension, { REACT_DEVELOPER_TOOLS } from "electron-devtools-installer";
import { parseAndValidateJSON, periscopeIconSmallPath } from "./main-utils";
import { readFile, writeFile } from "fs/promises";
import { ExportReturn } from "./types/ipc-api";
import { ExtraCertStore } from "./extra-cert-store";
import { FrontendApi } from "./frontend-api";
import { GuiWindowManagerImpl } from "./gui-window-manager";
import { HttpConditionManager } from "./http-condition/http-condition-manager";
import { ServerManager } from "./server-manager";
import { ServerStore } from "./server-store";
import { TLSCertificateManager } from "./tls-certificate-manager";
import { buildMenu } from "./menu";
import log from "electron-log/main";
import { showDialog } from "./dialog";

const logger = log.scope("App");

export class App {
    private readonly serverStore;
    private readonly certStore;
    private readonly conditionManager;
    private readonly serverManager;
    private readonly windowManager;
    private readonly frontendApi;
    private readonly certificateManager;
    private trayIcon?: Tray;

    constructor() {
        this.serverStore = new ServerStore();
        this.certStore = new ExtraCertStore();
        this.conditionManager = new HttpConditionManager();
        this.windowManager = new GuiWindowManagerImpl();
        this.frontendApi = new FrontendApi(this.windowManager);
        this.certificateManager = new TLSCertificateManager(this.serverStore);
        this.serverManager = new ServerManager(
            this.certStore,
            this.conditionManager,
            this.frontendApi,
            this.certificateManager,
        );

        app.whenReady()
            .then(async () => {
                await this.whenAppReady();
            })
            .catch((reason) => logger.error("Failed to start app", reason));
    }

    private async whenAppReady() {
        ipcMain.handle("saveSettings", (_: IpcMainInvokeEvent, settings: ProxySettings) => {
            logger.info("received settings from client");
            this.serverStore.save(settings);
        });

        ipcMain.handle("getSettings", () => {
            const store = this.serverStore.getAll();
            return Promise.resolve(store);
        });

        ipcMain.handle(
            "export-condition-sets",
            async (_: IpcMainInvokeEvent, conditionSetIds: string[]): Promise<Result<ExportReturn, string>> => {
                const conditionSets = this.serverStore
                    .getAll()
                    .conditionsSets.filter((set) => conditionSetIds.includes(set.id));
                const fileContents = JSON.stringify(conditionSets);
                const pathResult = await this.windowManager.openSaveDialog({
                    title: "Save Condition Sets",
                    filters: [{ name: "Condition Set Export", extensions: ["json"] }],
                });

                if (pathResult.err) {
                    logger.error(pathResult.val.message);
                    return Err(pathResult.val.message);
                }

                const pathData = pathResult.val;
                if (pathData.canceled) return Ok({ cancelled: true });

                // Should never happened according to the docs but better to check
                if (pathData.filePath == null) return Err("Failed to retrieve path");

                return await writeFile(pathData.filePath, fileContents)
                    .then(() => Ok({ cancelled: false }))
                    .catch((e) => {
                        logger.error(e);
                        return Err("Failed to save file");
                    });
            },
        );

        logger.info("App ready");
        ipcMain.handle("import-condition-set", async (_: IpcMainInvokeEvent): Promise<ConditionImportResult> => {
            const pathResult = await this.windowManager.openOpenFileDialog({
                title: "Import Condition Sets",
                filters: [{ name: "Condition Set Import", extensions: ["json"] }],
                properties: ["openFile"],
            });

            if (pathResult.err) {
                logger.error(pathResult.val.message);
                return FileError("Failed to open dialog");
            }

            const pathData = pathResult.val;
            if (pathData.canceled) return Cancelled();

            const filePath = pathData.filePaths[0];
            if (filePath == null) return FileError("Invalid path from dialog");

            const fileData = await readFile(filePath)
                .then((data) => Ok(data))
                .catch((err) => Err(err));

            if (fileData.err) {
                logger.error(fileData.err);
                return FileError("Failed to read file from path");
            }

            const parseResult = parseAndValidateJSON(fileData.val.toString(), ConditionSetSchema.array());

            if (parseResult.err) {
                return FileError(parseResult.val);
            }

            return FileData(parseResult.val.map(generateIdsForConditionSet));
        });

        ipcMain.handle("log-file-path", () => {
            return log.transports.file.getFile().path;
        });

        ipcMain.handle("open-log-file", () => {
            shell.showItemInFolder(log.transports.file.getFile().path);
        });

        ipcMain.handle("show-browse-dialog", async (_, options: FileDialogOptions): Promise<FileDialogResult> => {
            const { title, files, multiselect } = options;
            const properties: OpenDialogOptions["properties"] = ["openFile"];
            if (multiselect) {
                properties.push("multiSelections");
            }
            const dialogResult = await this.windowManager.openOpenFileDialog({ title, filters: files, properties });
            if (dialogResult.err) {
                return DialogError(dialogResult.val.message);
            }

            const selected = dialogResult.val;
            if (selected.canceled) {
                return Cancelled();
            }

            if (multiselect) {
                return MultiFilePath(selected.filePaths);
            }

            if (selected.filePaths.length > 1) {
                log.warn("Multiple files seleted even though only one was requested");
            }

            return FilePath(selected.filePaths[0]);
        });

        await this.createWindow();
        this.setupStore();
        this.addTrayIcon();
        this.addMenu();
        this.installExtensions();
        this.setupLogging();

        app.on("window-all-closed", () => {
            /* Left intentionally empty. If it is not handled, app.quit is called */
        });

        app.on("quit", () => this.serverManager.stopAll());

        app.setClientCertRequestPasswordHandler(async ({ hostname, tokenName }): Promise<string> => {
            const parentWindow = BrowserWindow.getAllWindows().find((window) =>
                window.webContents.getURL().includes(hostname),
            );

            const result = await showDialog(
                "textInput",
                { question: `Enter PIN for ${tokenName}`, inputType: "password" },
                { parentWindow },
            );
            if (result.err) {
                log.error("Failed to get Cerfificate PIN", result.val);
                return "";
            }

            return result.val ?? "";
        });
    }

    private setupStore() {
        this.serverManager.newSettings(this.serverStore.getAll());

        this.serverStore.watchForChanges((newSettings) => {
            if (newSettings == null) {
                logger.warn("Watched for changes had undefined newSettings");
                return;
            }

            this.serverManager.newSettings(newSettings);
        });
    }

    private addTrayIcon() {
        this.trayIcon = new Tray(periscopeIconSmallPath());
        this.trayIcon.setToolTip("Periscope");
        this.trayIcon.setContextMenu(
            Menu.buildFromTemplate([
                {
                    label: "Open Settings",
                    type: "normal",
                    click: () => {
                        this.activateOrCreateWithError();
                    },
                },
                { type: "separator" },
                { label: "Exit", type: "normal", click: () => app.quit() },
            ]),
        );

        logger.debug("tray added");
    }

    private addMenu() {
        const menu = buildMenu(this.frontendApi);
        Menu.setApplicationMenu(menu);
    }

    private installExtensions() {
        if (app.isPackaged) return;

        installExtension(REACT_DEVELOPER_TOOLS)
            .then((name) => logger.debug("Added extension:", name))
            .catch((err) => logger.error("An error occurred installing an extension", err));
    }

    private setupLogging() {
        // Will automatically catch and log electron failure events
        log.eventLogger.startLogging();

        if (app.isPackaged) {
            log.transports.file.level = "info";
            log.transports.console.level = "info";
        }
    }

    private async createWindow() {
        await this.windowManager.activateOrCreate();
    }

    private activateOrCreateWithError() {
        this.windowManager.activateOrCreate().catch((err) => {
            logger.error("Failed to activate window", err);
            dialog.showErrorBox("Failed to activate window", "The settings window could not be opened");
        });
    }
}
