import "./scss/app.scss";
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";

import * as React from "react";
import * as ReactDOM from "react-dom/client";
import { NotificationContext, NotificationEventQueue } from "./app/notification-context";
import { RouterProvider, createHashRouter } from "react-router-dom";
import { Main } from "./app/main";
import { SelectorDialog } from "./app/selector-dialog";
import { TextInputDialog } from "./app/text-input-dialog";

const notificationQueue = new NotificationEventQueue();

const router = createHashRouter([
    {
        path: "/",
        element: <Main />,
    },
    // TODO: Make this common
    {
        path: "/textInput/:requestId",
        loader: async ({ params }) => {
            return await api.requestDialogData(params.requestId!);
        },
        element: <TextInputDialog />,
    },
    {
        path: "/selector/:requestId",
        loader: async ({ params }) => {
            return await api.requestDialogData(params.requestId!);
        },
        element: <SelectorDialog />,
    },
]);

ReactDOM.createRoot(document.querySelector("#app")!).render(
    <React.StrictMode>
        <NotificationContext.Provider value={notificationQueue}>
            <RouterProvider router={router} />
        </NotificationContext.Provider>
    </React.StrictMode>,
);
