import * as React from "react";
import { None, Option, Some } from "ts-results";
import Button from "@mui/material/Button";
import Checkbox from "@mui/material/Checkbox";
import { ConditionSet } from "../../types/server";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormGroup from "@mui/material/FormGroup";
import Typography from "@mui/material/Typography";

type OnExportClosed = (conditions: Option<string[]>) => void;
type EnabledConditions = { [conditionId: string]: boolean };
type DialogState = { conditions: EnabledConditions; errorMessage: Option<string> };

export const ExportDialog = ({
    conditionSets,
    shouldOpen,
    onExportClosed,
}: {
    conditionSets: ConditionSet[];
    shouldOpen: boolean;
    onExportClosed: OnExportClosed;
}) => {
    const generateState = React.useCallback(
        (): EnabledConditions =>
            conditionSets.reduce((collector, value) => {
                collector[value.id] = false;
                return collector;
            }, {} as EnabledConditions),
        [conditionSets]
    );

    const [state, setState] = React.useState<DialogState>({ conditions: {}, errorMessage: None });
    const { conditions, errorMessage } = state;

    React.useEffect(() => {
        setState({ errorMessage: None, conditions: generateState() });
    }, [generateState, shouldOpen]);

    const handleChange = (id: string, enabled: boolean) => {
        setState({ errorMessage: None, conditions: { ...conditions, [id]: enabled } });
    };

    const onCancel = () => {
        onExportClosed(None);
    };

    const onExportClicked = () => {
        const selectedIds = conditionSets.filter((set) => conditions[set.id] ?? false).map((set) => set.id);
        if (selectedIds.length < 1) {
            setState({ ...state, errorMessage: Some("Need to select at least one set") });
            return;
        }

        onExportClosed(Some(selectedIds));
    };

    return (
        <Dialog open={shouldOpen} onClose={onCancel}>
            <DialogTitle>Which condition sets should be exported?</DialogTitle>
            <DialogContent>
                <FormGroup>
                    {conditionSets.map((condition) => (
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={conditions[condition.id]}
                                    onChange={(event) => handleChange(condition.id, event.target.checked)}
                                ></Checkbox>
                            }
                            label={condition.name}
                            key={condition.id}
                        ></FormControlLabel>
                    ))}
                </FormGroup>
                {errorMessage.some && (
                    <Typography variant="body1" sx={{ color: "error.main" }}>
                        {errorMessage.val}
                    </Typography>
                )}
            </DialogContent>
            <DialogActions>
                <Button onClick={onCancel}>Cancel</Button>
                <Button onClick={onExportClicked}>Export</Button>
            </DialogActions>
        </Dialog>
    );
};
