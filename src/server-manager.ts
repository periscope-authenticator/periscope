import { None, Some } from "ts-results";
import { ProxySettings, Server, ServerStatus } from "./types/server";

import { HeaderTransform } from "./header-transform";
import { HttpConditionManager } from "./http-condition/http-condition-manager";
import { HttpProxyInfoImpl } from "./http-proxy-info";
import { IExtraCertStore } from "./extra-cert-store";
import { IFrontendAPI } from "./frontend-api";
import { ProxyServer } from "./proxy-server";
import { TLSCertificateManager } from "./tls-certificate-manager";
import { ipcMain } from "electron";
import { isBlank } from "./utils";
import log from "electron-log/main";

export interface IServerManager {
    stopAllAsync: () => Promise<void[]>;
    stopAll: () => void;
    localForRemoteAddress: (hostToFind: URL, hostFromClient: string) => string;
    serverStatuses(): ServerStatus[];
}

const logger = log.scope("ServerManager");

/**
 * Contains ProxyServers and manages changes from the settings
 */
export class ServerManager {
    private servers: Map<string, ProxyServer>;
    private certStore: IExtraCertStore;

    constructor(
        certStore: IExtraCertStore,
        private conditionManager: HttpConditionManager,
        private frontendApi: IFrontendAPI,
        private certificateManager: TLSCertificateManager,
    ) {
        this.servers = new Map<string, ProxyServer>();
        this.certStore = certStore;
        ipcMain.handle("get-server-status", () => this.serverStatuses());
        ipcMain.handle("start-server", (_, serverId: string) => this.startServer(serverId));
        ipcMain.handle("stop-server", (_, serverId: string) => this.stopServer(serverId));
    }

    /**
     * Stops all the servers and awaits for the servers to stop
     * @returns Promise that resolves when all servers have finished stopping
     */
    public stopAllAsync(): Promise<void[]> {
        const promises: Promise<void>[] = [];
        this.servers.forEach((proxyServer) => {
            promises.push(proxyServer.shutdownAsync());
        });

        return Promise.all(promises);
    }

    /**
     * Stops all the servers and waits for the servers to stop
     */
    public stopAll() {
        this.servers.forEach((proxyServer) => {
            proxyServer.shutdown();
        });
    }

    /**
     * Apply new settings to the servers
     * @param settings New set of settings to apply to the servers
     */
    public newSettings(settings: ProxySettings) {
        this.conditionManager.updateSettings(settings);

        const servers = settings.servers ?? [];
        this.stopDeletedServers(servers);

        const proxyData = isBlank(settings.proxyUrl) ? None : Some(new HttpProxyInfoImpl(new URL(settings.proxyUrl)));

        servers.forEach((server) => {
            const proxyServer = this.servers.get(server.id);

            if (proxyServer === undefined) {
                this.addServer(server);
                return;
            }

            proxyServer
                .updateSettings(server, proxyData)
                .catch((e) => logger.error(`Failed to update proxy server ${server.name}\n${e}`));
        });

        this.updateStatuses();
    }

    /**
     * Converts a host to the proxied version on the URL if it is a host we are proxying
     * @param hostToFind Host we want to find a proxy address for
     * @param hostFromClient Host the client thinks Periscope is on
     * @returns a host if it is a proxied host. original host otherwise
     */
    public localForRemoteAddress(hostToFind: URL, hostFromClient: string): string {
        let serverWithURL: ProxyServer | null = null;

        for (const [_, server] of this.servers) {
            if (server.serverInfo.proxyAddress === hostToFind.hostname) {
                serverWithURL = server;
                break;
            }
        }

        if (serverWithURL === null) {
            return hostToFind.toString();
        }

        const newURL = new URL(hostToFind);
        // The host may not be localhost e.g. Vagrant/VM so we want to take what the client thinks the host is
        newURL.host = hostFromClient;
        // Periscope only supports http
        newURL.protocol = "http";
        newURL.port = String(serverWithURL.serverInfo.listenPort);

        return newURL.toString();
    }

    public serverStatuses(): ServerStatus[] {
        return Array.from(this.servers).map(([_, server]) => server.status());
    }

    public startServer(serverId: string) {
        this.servers.get(serverId)?.startListening();
    }

    public stopServer(serverId: string) {
        this.servers.get(serverId)?.shutdown();
    }

    /**
     * Adds the server to the storage and starts it
     * @param newServerInfo Server info to add
     */
    private addServer(newServerInfo: Server) {
        try {
            const newServer = new ProxyServer(
                newServerInfo,
                this.certStore,
                this.conditionManager,
                new HeaderTransform(this),
                this.certificateManager,
                () => this.updateStatuses()
            );

            newServer.startListening();
            this.servers.set(newServerInfo.id, newServer);
        } catch (e) {
            logger.error(`Failed to add a server for ${newServerInfo.name}`, e);
        }
    }

    /**
     * Stops all servers which are not part of the updated list
     * @param newServers New set of servers
     */
    private stopDeletedServers(newServers: Server[]) {
        const serverSet = new Set(newServers.map((server) => server.id));

        this.servers.forEach((proxyServer) => {
            if (!serverSet.has(proxyServer.id)) {
                this.deleteServer(proxyServer);
            }
        });
    }

    // TODO: This updates all statuses when only one has changed which causes more status
    // changes than necessary
    private updateStatuses() {
        logger.debug("Updating the statuses");
        this.frontendApi.notifyServerStatusUpdated(this.serverStatuses());
    }

    /**
     * Shuts down and deletes the server once the shutdown is complete
     * @param server Server to delete
     */
    private deleteServer(server: ProxyServer) {
        logger.info(`Deleting server ${server.name}`);
        server
            .shutdownAsync()
            .then(() => this.servers.delete(server.id))
            .catch(() => logger.error(`Failed to shutdown ${server.name}`));
    }
}
