import { GuiWindowManager } from "./gui-window-manager";
import { ServerStatus } from "./types/server";

export interface IFrontendAPI {
    notifyServerStatusUpdated: (status: ServerStatus[]) => void;
    openAboutMenu: () => void;
}

export class FrontendApi implements IFrontendAPI {
    constructor(private windowManager: GuiWindowManager) { }

    notifyServerStatusUpdated(status: ServerStatus[]) {
        this.windowManager.sendToWindow("new-server-status", status);
    }

    openAboutMenu() {
        this.windowManager.sendToWindow("show-about-menu");
    }
}
