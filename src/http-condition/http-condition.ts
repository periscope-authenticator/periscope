import * as http from "node:http";

import {
    ConditionStore,
    NumberComparatorFuncs,
    NumberComparatorType,
    NumberComparators,
    StringComparatorFuncs,
    StringComparatorType,
    StringComparators,
} from "./condition-store";

export interface HttpCondition {
    shouldRetrieveCookies(serverResponse: http.IncomingMessage): boolean;
}

class StatusCodeCondition {
    constructor(
        private compareValue: number,
        private comparator: NumberComparatorType,
    ) {
        if (!NumberComparators.includes(comparator)) {
            throw new Error("comparator is not valid");
        }
    }

    shouldRetrieveCookies(serverResponse: http.IncomingMessage): boolean {
        return NumberComparatorFuncs[this.comparator](serverResponse.statusCode!, this.compareValue);
    }
}

class HeaderCondition {
    private headerName: string;
    constructor(
        headerName: string,
        private comparator: StringComparatorType,
        private value: string,
    ) {
        if (!StringComparators.includes(comparator)) {
            throw new Error("comparator is not valid");
        }

        this.headerName = headerName.toLowerCase();
    }

    shouldRetrieveCookies(serverResponse: http.IncomingMessage): boolean {
        const headerValue = serverResponse.headers[this.headerName];

        if (typeof headerValue === "string" || headerValue == null) {
            // The above should cover if it is a string but Typescript doesn't seem to think so
            return StringComparatorFuncs[this.comparator](headerValue as string, this.value);
        }

        return headerValue.some((value) => StringComparatorFuncs[this.comparator](value, this.value));
    }
}

export function buildConditionsFromStore(storedFilters: ConditionStore[]): HttpCondition[] {
    return (storedFilters ?? []).map((store) => {
        if (store.conditionSource === "header") {
            return conditionFromHeader(store);
        } else if (store.conditionSource === "response-code") {
            return conditionFromStatusCode(store);
        }

        throw new Error("Unknown condition source");
    });
}

function conditionFromHeader(store: ConditionStore): HttpCondition {
    if (store.headerComparator == null || store.headerName == null || store.headerValue == null) {
        throw new Error("comparator is not valid");
    }

    return new HeaderCondition(store.headerName, store.headerComparator, store.headerValue);
}

function conditionFromStatusCode(store: ConditionStore): HttpCondition {
    if (store.numberComparator == null || store.numberValue == null) {
        throw new Error("comparator is not valid");
    }

    return new StatusCodeCondition(store.numberValue, store.numberComparator);
}
