import { z } from "zod";

const BaseComparators = ["==", "!="] as const;

export const StringComparators = [
    "contains",
    "not containing",
    "starts with",
    "ends with",
    ...BaseComparators,
] as const;

const StringComparatorsSchema = z.enum(StringComparators);
export type StringComparatorType = z.infer<typeof StringComparatorsSchema>;

export const NumberComparators = ["<", "<=", ">", ">=", ...BaseComparators] as const;

const NumberComparatorSchema = z.enum(NumberComparators);
export type NumberComparatorType = z.infer<typeof NumberComparatorSchema>;

// This is probably a little over-engineered. However it guarantees that the comparators in the arrays above
// are actually reflected in the below function objects.
type INumberCompFuncs = {
    [Property in NumberComparatorType]: (statusCode: number, compareValue: number) => boolean;
};

type IStringCompFuncs = {
    [Property in StringComparatorType]: (value: string | null, compareValue: string) => boolean;
};

export const NumberComparatorFuncs: INumberCompFuncs = {
    "==": (statusCode: number, compareValue: number): boolean => statusCode === compareValue,
    "!=": (statusCode: number, compareValue: number): boolean => statusCode !== compareValue,
    "<": (statusCode: number, compareValue: number): boolean => statusCode < compareValue,
    "<=": (statusCode: number, compareValue: number): boolean => statusCode <= compareValue,
    ">": (statusCode: number, compareValue: number): boolean => statusCode > compareValue,
    ">=": (statusCode: number, compareValue: number): boolean => statusCode >= compareValue,
};

export const StringComparatorFuncs: IStringCompFuncs = {
    "==": (value: string | null, compareValue: string): boolean => value === compareValue,
    "!=": (value: string | null, compareValue: string): boolean => value !== compareValue,
    contains: (value: string | null, compareValue: string): boolean => value?.includes(compareValue) ?? false,
    "not containing": (value: string | null, compareValue: string) =>
        !StringComparatorFuncs.contains(value, compareValue),
    "starts with": (value: string | null, compareValue: string): boolean => value?.startsWith(compareValue) ?? false,
    "ends with": (value: string | null, compareValue: string): boolean => value?.endsWith(compareValue) ?? false,
};

const ConditionSourceArray = ["header", "response-code"] as const;
const ConditionSourceSchema = z.enum(ConditionSourceArray);
export type ConditionSources = z.infer<typeof ConditionSourceSchema>;

const HeaderConditionOptions = z.object({
    headerName: z.string().nullish(),
    headerComparator: StringComparatorsSchema.nullish(),
    headerValue: z.string().nullish(),
});

const NumberOptions = z.object({
    numberValue: z.number().nullish(),
    numberComparator: NumberComparatorSchema.nullish(),
});

export const ConditionStoreSchema = NumberOptions.extend(HeaderConditionOptions.shape).extend({
    conditionSource: ConditionSourceSchema,
});

export type ConditionStoreInput = z.infer<typeof ConditionStoreSchema>;
export type ConditionStore = ConditionStoreInput & { id: string };
